package com.mentorenterprisesinc.boardmaster;

import java.util.ArrayList;
import java.util.Random;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Toast;

public class Flashcard_Activity extends FragmentActivity implements UpdateableFragment {

	private static final String LOGTAG="Flashcard_Activity";
	//will hold the number of flash cards mdail 5-29-15
	private static int NUM_PAGES;
    // The pager widget, which handles animation and allows swiping horizontally to access previous and next wizard steps mdail 5-29-15
	public ViewPager pager;
	public ScreenSlidePagerAdapter adapter;
	public Boolean dataUpdated = false;
	public Boolean cardDataUpdated = false;
	public MenuItem saveCardData;
	public MenuItem editCardData;
	private Context context;
	
	@Override
	public void UpdateFlashList() {
    	adapter.notifyDataSetChanged();
	}
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.flash_cards_activity);
		//Set the Icon as a back button mdail 8-10-15
		getActionBar().setDisplayHomeAsUpEnabled(true);
		this.context = this;
        // Instantiate a ViewPager and a PagerAdapter.
        adapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        pager = (ViewPager)findViewById(R.id.pager);
        pager.setAdapter(adapter);
		setListeners();
	}
	//Used to restore the card # if the help button is pressed, the app get set out of focus by another app, 
	//Orientation changes (when I have time to add that option) etc. mdail 8-18-15
	@Override
	protected void onSaveInstanceState(Bundle outState) {
	    super.onSaveInstanceState(outState);
	}
	
	private void setListeners() {
		//get PREVIOUS CHAPTER mdail 6-1-15
		ImageButton button = (ImageButton)findViewById(R.id.btn_prev_ch);
		button.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if(Chapters.favorite) {
					Integer i = pager.getCurrentItem();
					if(i > 0) {
						i = 0;
						//This should set the currently displayed to 0
						pager.setCurrentItem(i,false);
						//by returning I don't need an else, the code past this will not run unless the current position is 0 mdail 7-29-15
						return;
					}
					Context context = getApplicationContext();
					Toast toast = Toast.makeText(context, "This is the first selected question of the favorites!", Toast.LENGTH_SHORT);
					toast.show(); 
					return;
				}else {
					Integer i = pager.getCurrentItem();
					//if I is less than the position the first chapter ends at then just go to position 0 mdail 7-29-15
					if(i < Chapters.chapterEndsAt[0]) {
						i = 0;
						//This should set the currently displayed to 0
						pager.setCurrentItem(i,true);
						//by returning I don't need an else, the code past this will not run unless the current position is 0 mdail 7-29-15
						return;
					}
					//gets the start of the previous chapter
					i = getPrevChapterStart(i);
					if(i.equals(-1)) {
						Context context = getApplicationContext();
						Toast toast = Toast.makeText(context, "This is the first selected question of the first selected chapter!", Toast.LENGTH_SHORT);
						toast.show(); 
						return;
					}
					//should move back one chapter mdail 5-29-15
					pager.setCurrentItem(i,false);
				}
			}
		});
		//get PREVIOUS QUESTION mdail 6-1-15
		button = (ImageButton)findViewById(R.id.btn_prev);
		button.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Integer i = 0;
				//gets the current position mdail 6-1-15
				i = pager.getCurrentItem();
				i--;
				if(i.equals(-1)) {
					Context context = getApplicationContext();
					Toast toast = Toast.makeText(context, "This is the first selected question!", Toast.LENGTH_SHORT);
					toast.show(); 
					return;
				}
				//Should move go back one cards mdail 6-1-15
				pager.setCurrentItem(i,true);
			}
		});
		//get NEXT RANDOM mdail 6-1-15
		button = (ImageButton)findViewById(R.id.btn_rand);
		button.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if(Chapters.favorite) {
					int i = 1;
					Random rand = new Random();
					//generate a random number with the total number of questions as the upper limit mdail 6-1-15
					i= rand.nextInt(Chapters.favorites.size());
					//move to a random question
					pager.setCurrentItem(i,false);
				}else {
					int i = 1;
					i = Chapters.totalQuestions;
					Random rand = new Random();
					//generate a random number with the total number of questions as the upper limit mdail 6-1-15
					i= rand.nextInt(i);
					//move to a random question
					pager.setCurrentItem(i,false);
				}
			}
		});
		//get NEXT QUESTION mdail 6-1-15
		button = (ImageButton)findViewById(R.id.btn_next);
		button.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Integer i = 0;
				//gets the current position mdail 6-1-15
				i = pager.getCurrentItem();        	
				i++;
				if(i.equals(-1) || i >= NUM_PAGES) {
					Context context = getApplicationContext();
					Toast toast = Toast.makeText(context, "This is the last selected question!", Toast.LENGTH_SHORT);
					toast.show(); 
					return;
				} 
				//should move forward one card mdail 5-29-15
				pager.setCurrentItem(i,true);
			}
		});
		//get NEXT CHAPTER mdail 6-1-15
		button = (ImageButton)findViewById(R.id.btn_next_ch);
		button.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if(Chapters.favorite) {
					Integer i = pager.getCurrentItem();
					//if we are not at the last question go to the last question, otherwise let the user know they are already at the last 
					//question mdail 7-29-15 
					if(i < (Chapters.favorites.size() - 1)) {
						pager.setCurrentItem((Chapters.favorites.size() - 1),false);
						return;
					}
					Context context = getApplicationContext();
					Toast toast = Toast.makeText(context, "You are already at the last question of the favorites!", Toast.LENGTH_SHORT);
					toast.show(); 
					return;
				}else {
					Integer i = pager.getCurrentItem();
					i = getNextChapterStart(i); 
					if(i.equals(-1) || i.equals(0) || i >= NUM_PAGES){
						Context context = getApplicationContext();
						Toast toast = Toast.makeText(context, "You are already at the start of the last selected chapter!", Toast.LENGTH_SHORT);
						toast.show(); 
						return;
					}
					//should move forward one chapter mdail 5-29-15
					pager.setCurrentItem(i,false);
				}
			}
		});
	}
	//This function calculates the position of the start of the previous chapter and returns is so we can scroll to the previous chapter
	//if there are any errors or we are in the first chapter, or if the new position is the same or less than a position in the first chapter we are going
	//to return we return -1 which the click handler uses to display a message tell the user that they are already in the first chapter so we can't 
	//go to the previous chapter mdail 6-3-15
	private Integer getPrevChapterStart(Integer position) {
		Integer r = 1;
		if(position.equals(0)) {
			return -1;
		}
		while(position >= Chapters.chapterEndsAt[r]) {
			r++;
			if(r.equals(21)) {break;}
		}
		//if we are in the first chapter, check to see if we are at the first question, if we are return -1, if not return 0 mdail 7-29-15
		if(Chapters.selectedChaptersForFlash[r].equals(Chapters.firstSelectedChapter)) {
			if(position > 0) {
				return 0;
			}
			return -1;
		} 
		//if r == 2 then we are only on the second chapter so we go to the first question mdail 6-1-15 
		if(r.equals(2)) {		 
			return 0;
		}
		//if we get to here with the chapter (c) we find the last question of the 2 chapters before the one we are currently on mdail 6-1-15
		return Chapters.chapterEndsAt[r - 2];
	}
	//This function calculates the position of the start of the next chapter and returns is so we can scroll to the next chapter
	//if there are any errors or we are in the last chapter, or if the new position is the same or larger than the position we are going
	//to return we return -1 which the click handler uses to display a message tell the user that they are no more chapters so we can't 
	//go to the next chapter mdail 6-3-15
	private Integer getNextChapterStart(Integer position) {
		Integer c = 1;
		Integer r = 1;
		//get the index for the chapters mdail 6-3-15
		while(position >= Chapters.chapterEndsAt[r]) {
			r++;			
			if(r.equals(23)) {
				return -1;
			}
		}	
		// returning 0 triggers the on click to display a toast message that says that there is no next chapter mdail 6-1-15
		if(r >= 22) {	
			int q=0;
			q=Chapters.chapterEndsAt[r - 1];
			if(position != q) {	
				return q;
			}
			return -1;
		}
		//if we are in the last chapter however not at the first card of the last chapter go to the last card of all the cards chosen, 
		//if we are at the last card return -1, which trigers a toast message that says we are already at the last question 
		//return -1 mdail 7-29-15
		if(Chapters.selectedChaptersForFlash[r].equals(Chapters.lastSelectedChapter)) {	
			Integer q=0;
			q.equals(Chapters.chapterEndsAt[r - 1]);
			if(position != q) {
				if(position.equals(Chapters.totalQuestions - 1)) {
					return -1;
				}else {
					return Chapters.totalQuestions - 1;
				}
			}
			return -1;
		}  
		//If we get this far r should be the chapter we are in, so we get the last question (which due to 0 index is really the first card of the next 
		// chapter) and that should be the first question of the next chapter mdail 6-1-15
		c = Chapters.chapterEndsAt[r];
		//Check to make sure c is not the same or larger than the total number of questions selected mdail 6-2-15
		if(c >= Chapters.totalQuestions) 
		{
			return -1;
		}
		return c;
	}	
	@Override
	protected void onStart(){
		Log.i(LOGTAG, "onStart");
		super.onStart();
	}
	@Override
	protected void onResume(){
		Log.i(LOGTAG, "onResume");
		super.onResume();
	}
	@Override 
	public void onPause() {
		Log.d(LOGTAG, "onPause:");
		super.onPause();
	}
	@Override 
	public void onStop() {
		Log.d(LOGTAG, "onStop:");
		super.onStop();
	}
	@Override 
	public void onRestart(){
		super.onRestart();
		Log.d(LOGTAG, "onRestart:");
	}
	@Override 
	public void onDestroy() {
		Log.i(LOGTAG, "onDestroy");
		super.onDestroy();
	}
	//goes back to the chapter list if the back button is pressed mdail 5-29-15
	@Override
	public void onBackPressed() {
		ProgressBar pb_flash = (ProgressBar)findViewById(R.id.pb_flash);
		pb_flash.setVisibility(View.VISIBLE);
		Chapters.favorite = false;
		Chapters.favorites = null;
		Chapters.favorites = new ArrayList<Cards>();
		FlashCardsDataSource datasource = new FlashCardsDataSource(this);
		datasource.getFavoriteCards();
		datasource = null;
		pb_flash.setVisibility(View.INVISIBLE);
		finish();
    }
	@Override
	public boolean onPrepareOptionsMenu (Menu menu) {
	    return true;
	}
    @Override
	public boolean onCreateOptionsMenu(Menu menu) {
    	MenuInflater inflater = getMenuInflater();
    	Integer screenWidth = SharedProcedures.getScreenWidth(context);
    	Integer screenHeight = SharedProcedures.getScreenHieght(context);
    	Integer screenSize = 0;
		if(screenWidth < screenHeight) {
			screenSize = screenWidth;
		}else {
			screenSize = screenHeight;
		}
    	if(screenSize <= 240) {
    		inflater.inflate(R.menu.flashcard_actionbar_smallest, menu);
    	}
    	if(screenSize > 240 && screenSize <= 320) {
    		inflater.inflate(R.menu.flashcard_actionbar_small, menu);
    	}
    	if(screenSize > 320 && screenSize <= 480) {
    		inflater.inflate(R.menu.flashcard_actionbar_medium, menu);
    	}
    	if(screenSize > 480 && screenSize <= 600) {
    		inflater.inflate(R.menu.flashcard_actionbar_large, menu);
    	}
    	if(screenSize > 600) {
    		inflater.inflate(R.menu.flashcard_actionbar_xlarge, menu);
    	}
        editCardData = menu.findItem(R.id.btn_edit);
        saveCardData = menu.findItem(R.id.bnt_save);        
        return super.onCreateOptionsMenu(menu);
	}

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	// Handle action bar item clicks here. The action bar will
    	// automatically handle clicks on the Home/Up button, so long
    	// as you specify a parent activity in AndroidManifest.xml.
    	int id = item.getItemId();
    	 switch (id) {
         case R.id.btn_edit:
        	 return false;
         case R.id.bnt_save:
        	 return false;
         case R.id.bnt_cancel:
    	 	return false;
         case R.id.bnt_help:
			Intent intent = new Intent(Flashcard_Activity.this,Help_Flash_Cards.class);
			startActivity(intent);
    	 	return true;
         case android.R.id.home:
         	//code to exicute the back action from the icon mdail 5-29-15
      		ProgressBar pb_flash = (ProgressBar)pager.findViewById(R.id.pb_flash);
      		pb_flash.setVisibility(View.VISIBLE);
      		Chapters.favorite = false;
      		Chapters.favorites = null;
      		Chapters.favorites = new ArrayList<Cards>();
      		FlashCardsDataSource datasource = new FlashCardsDataSource(this);
      		datasource.getFavoriteCards();
      		datasource = null;
      		pb_flash.setVisibility(View.INVISIBLE);
      		finish();
            return true;    		
         default:
             return super.onOptionsItemSelected(item);
    	 }
     }
    /**
     * A simple pager adapter that represents ScreenSlidePageFragment objects, in
     * sequence. mdail  5-29-15
     */
    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
            	
    	public ScreenSlidePagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }
        
        @Override
        public Fragment getItem(int position) {
        	//Check to see if we are trying to go past the last question
        	if(position >= NUM_PAGES) {
    			Context context = getApplicationContext();
    			Toast toast = Toast.makeText(context, "You are at the last question!", Toast.LENGTH_SHORT);
    			toast.show(); 
    			return null;
        	}        	
        	//Extends the Flashcard_Fragment  class
            Fragment fragment = new Flashcard_Fragment();
			Bundle args = new Bundle();
			args.putInt(Flashcard_Fragment.ARG_SECTION_NUMBER, position);
			fragment.setArguments(args);
			return fragment;
        }

        @Override
        public int getCount() {
        	if(!Chapters.favorite) {
        		try {
        			NUM_PAGES = Chapters.totalQuestions;
        		}catch(Exception e) {
        			Log.d(LOGTAG,"catch e = " + e.getLocalizedMessage());
        		}
        	}else {
        		try {
        			NUM_PAGES = Chapters.favorites.size();
        		}catch(Exception e) {
        			Log.d(LOGTAG,"catch e = " + e.getLocalizedMessage());
        		}
        	}
        	return NUM_PAGES;
        }
        //when data has been updated then POSITION_NONE is returned to tell the adapter to change the data mdail 7-28-15
        @Override
        public int getItemPosition(Object object) {
            if(dataUpdated) {
            	dataUpdated = false;
            	return POSITION_NONE;
            }else {
            	return super.getItemPosition(object);
            }
        }

    }
   
}

