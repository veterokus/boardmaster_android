package com.mentorenterprisesinc.boardmaster;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
/**
 * Created by mdail on 9-21-15.
 * the module is to keep track of and return the version
 * number
 */
public class VersionNumber extends Activity {

	public VersionNumber(){
		//Have to initialize the connector to solve static non-static problems
		//to use in an Activity or anywhere else, use the two lines of code that follow 
		//NOTE: do not uncomment them here as that would cause errors simply add the to were ever you want to use the Version Number
		//mdail 5-8-15
		//VersionNumber v = new VersionNumber();
		//setTitle("PDG Ultimate Version No: " + v.getVersionNumber(this));
	}

	public static String getVersionNumber(Context context) {

		try {
			PackageManager pm = context.getPackageManager();
			PackageInfo packageInfo = pm.getPackageInfo(context.getPackageName(), 0);
			return packageInfo.versionName;
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		return "";
	}
}



//v15.023.0121  Set up for further updaes down the line when needed mdaail 5-26-16 
//v14.023.0121  Also fixed some minor layout issues mdail 5-26-16
//v14.023.0120  Fixed bug in license check and fixed key mdail 5-26-16
//v14.023.0119  Turn licensing back of and prepair for next version number mdail 4-27-16
//v13.023.0118  Fixed crash and added licensing check mdail 4-27-16
//v12.023.0117  Added the new database and made sure it would copy itself over the old database, tried to fix the cause of the 
//              3 Crashes reported in the last 90 day before it becomes a problem mdail 4-22-16
//v12.023.0116  Updated to new database to have more current information mdail 11-3-15
//v11.022.0115	Updated to new licensing key as it was presented on the website after uploading the new apk mdail 11-3-15
//v11.022.0114	Turn licensing back on mdail 11-3-15
//v11.022.0113	Fix some miss errors like text size on edit text in the 720dp being wrong and things like that mdail 11-3-15
//v11.022.0112	Try to change the color of the text in the action bar and the drop down menu of the action bar mdail 11-3-15
//v11.022.0111	Finished the colors on the help page, also changed the color of the copyright text on the main page mdail 11-3-15
//v11.022.0110	Added the subject name to the flash cards also changed the text from white to gold, as am changing the help screen text from
//				white to gold mdail 11-2-15
//v11.021.0109	Added the chapter name (aka subject as when posting screen shots I noticed we didn't have them) mdail 11-2-15
//v10.021.0108	Added margins to the help screen and change the image size of the help, edit, save and quit buttons
//				also made it so when edit is hit and the keyboard is displayed, that when either save or quit are 
//				pressed the keyboard hide itself
//v10.021.0107	Added extra padding around the chapter select list, and preset the hint for the question in chapter 21 and both
//				question and answer in chapter 22, and modified the color as per Matthew's request mdail 10-19-15
//v10.021.0106	Compile with proguard mdail 10-19-15
//v10.021.0105  Removed the debugging code for most places in the app mdail 10-19-15
//v10.021.0104	Fixed bug that if the select all was checked the first time it was run it stayed checked mdail 10-19-15
//v10.021.0103	Fixed bug that if you hit flip with the edit_question on the screen, it would leave it visible and 
//				not flip to the answer mdail 10-19-15
//v10.021.0102	Fixed null pointer by setting the rest array to true if the 0 element is true mdail 10-19-15
//v10.021.0101	Set it so if no chapters are selected the it defaults to all chapters selected for the start 
//				button on the main screen mdail 10-19-15
//v10.021.0100	Fix the quit smallest & medium selector files pointing to the wrong sizes mdail 10-19-15
//v10.021.0099	made sure I had the correct BASE64_PUBLIC_KEY mdail 10-19-15
//v10.021.0098	Added new salt number for the licensing part of the app mdail 10-19-15
//v10.021.0097	Finish adding licensing to the project however it is disabled for the beta release, also add the permission 
//				needed for licensing sill need new salt number and to check BASE64_PUBLIC_KEY mdail 10-19-15
//v10.021.0096	Started to add the licensing to the project, I still need to finish and get new salt number, also check to see if
//				BASE64_PUBLIC_KEY is the same for all apps or whether I need a different one for the project mdail 10-16-15
//v10.020.0095	Fixed an index out of bounds mdail 10-16-15
//v10.020.0094	Set up a SharePreferences file and save and restore the list selection in the select chapters activity, also
//				use the same file to choose the chapters and go straight to the flash card activity from the start button on
//				the mainActivity mdail 10-16-15
//v10.019.0093	Really fix index out of bounds error and a couple of other errors that popped up as a result of adding a chapter
//				to the database mdail 10-16-15
//v10.019.0092	Fixed index out of bounds error trying to load all the chapters for flash cards mdail 10-16-15
//v10.019.0091	Fix select all not working mdail 10-16-15
//v10.019.0090	Finished updating the project I hope for the new database mdail 10-16-15
//v10.019.0089	Found the newer project and it was versionCode 9 so I updated mine to versionCode 10 and the version number
//				to start with 10 also mdail 10-16-15
//v7.019.0088	When I checked the IPad to see what the start button did, I found that I had an old database and there are
//				22 chapters so will waiting to get the new database I started updating the program to handle the new database
//				with 22 chapters. Hopefully this will not cause me to much greif mdail 10-16-15
//v7.018.0087	Fixed the problem of the save button staying visible if the card was scrolled after save had been pressed, and
//				in chapter 20 if the answer was showing, the edit button would be showing if the card was scrolled away from
//				and then back to mdail 10-16-15
//v7.018.0086	Did Code clean up removing unused variables and imports mdail 10-16-15
//v7.018.0085	fixed the final (I think) bugs in the extra activity page so after testing on other versions of android it might
//				be ready to put in beta after I figure out whether the start button should go to the select subjet activity
//				of simply select all subject and go mdail 10-16-15
//v7.018.0084	Added the landscape layout for the extras activity and made both layouts scroll views just in case mdail 10-16-15
//v7.018.0083	Moved the strings for the extras to their own class, also added a title text box to the layout for the 
//				extras, also in the strings class will be the titles for the extras mdail 10-16-15
//v7.018.0082	Added the activity that will display all of the extras and started working on it, I pass the number of
//				the item in the list the set the text using that number mdail 10-15-15
//v7.017.0081	I did have to make a second layout for the extras for land scape to keep the title on top mdail 10-15-15
//v7.017.0080	Working on getting the listView for the  Extras to dynamically resize itself mdail 10-15-15
//v7.017.0079	Started working on the ListView that will allow for choosing from the list of extras mdail 10-14-15 
//v7.016.0078	Also had to fix the save and cancel buttons staying visible if the cards flipped with them visible mdail 10-14-15
//v7.016.0077	Fixed edit and save as they would allow saving of blank strings, 
//v7.016.0076	Had to replace the buttons with the medium versions on the help screen mdail 10-14-15
//v7.016.0075	Finished the help activity and had to go back into the menu and enable the help button mdail 10-14-15
//v7.016.0074	Added the help activity for the flash cards and started setting up the layout for the help screen mdail 10-14-15
//v7.015.0073	Changed it so we get both screen height and width and the smaller of the two determines the menu mdail 10-14-15
//v7.015.0072 	Made multiple size icon for the action bar, menu's for the action bar, and am using the screen width to choose 
//				which one to use so the icon will be the icons right sizes mdail 10-14-15
//v7.015.0071	I am going to have to come up with a better way to do this, I reverted back to the way it was before I started
//				trying to make it only show one empty card at a time mdail 10-13-15
//v7.015.0070	Trying to make sure we only show one of the blank cards, then as the user fills them in we add more to the view mdail 10-13-15
//v7.015.0069	Fixed error in the loop that let it go too far looking the the end of the chapter 21 with question info mdail 10-13-15
//v7.015.0068	Add the code to limit chapter 21 to showing one blank question, then updating it when a new one has been edited mdail 10-13-15
//v7.015.0067	That did not work well either, I think I will end up having to set the image by reading the screen size and
//				picking the image by that. I fixed so layouts that I hadn't run yet, there are still some to test mdail 10-9-15
//v7.015.0066	Instead of using file names for the different size screen in the menu, I had to put the images in the size
//				folder and use them as one name for each of the 5 files and hope android really can figure out when to use
//				which size, note the cancel button has tested well and works mdail 10-9-15
//v7.015.0065	Had to fix the size of two of the buttons to be a little small as the were getting made way too small mdail 10-8-15
//v7.015.0064	Made icons for the Action Bar menu and a selector for the help button to see if it works mdail 10-9-15
//v7.015.0063	Added cancel button to cancel the edit in the menu of the flash fragment mdail 10-9-15
//v7.015.0062	Test .0061 and see if the button I made and downloaded in a selector will work mdail 10-8-15
//v7.015.0061	It works, except if you hit edit then page to the next cards, I think I might have just fixed that mdail 10-8-15
//v7.015.0060	Fixed two arrays in the chapters class that I had forgotten to make big enough for the longer list mdail 10-8-15
//v7.015.0059	Fixed an error in the select subject activity mdail 10-8-15
//v7.015.0058	instead of using a test to see if the question textView is visible to determine what to update I am now using
//				a boolean variable as I watched it fail when the textView was not visible mdail 10-8-15
//v7.015.0057	Try to set a boolean variable in the activity so when the fragment goes through onCreateView again it will see that
//				the data has been updated and hide the questions widget if it was the answer that was updated mdail 10-8-15
//v7.015.0056	Had to change to just using text for the edit and save in the action bar however that part is now at least
//				working, And I know that the save data work because when I had the enable true and false backwards I change the 
//				question for chapter 19 q 1 however I don't know if it will still flip when I edit the answer mdail 10-8-15
//v7.015.0055	Before I eve tried doing the save and edit from the activity I added the menu options to the 
//				fragment. I also found and am trying some options to have the methods to show, hide, save and edit only 
//				when the fragment becomes visible. I also had the idea to use the interface to update the fragment
//				in the middle of the save code for the answer, and then change the visibilities back to the answer after mdail 10-8-15
//v7.014.0054	Removed the edit and save buttons from fragment and added them to the action bar as menu items
//				now I have to move the edit and save code to here and see if I can get it working mdail 10-7-15
//v7.013.0053	Fixed the problem for the answer, now in ch 21 the question has a problem, the editText does not 
//				go invisible and the app crashes when you try to move to the next card. I am trying moving the update
//				code that cause the answer problem to the back to the question mdail 10-7-14
//v7.013.0052	Try calling the update fragment interface only after the card becomes invisible itself mdail 10-7-15
//v7.013.0051	Moved the updated variable to the activity instead of the fragment mdail 10-7-15
//v7.013.0050	The work from .0048 might have made the work from .0049 look like it did not work mdail 10-7-15
//v7.013.0049	Using the boolean return from update view to hide the question if the answer has been edited mdail 10-7-15	
//v7.013.0048	Added a dialog that tells the user that they can not go to the next card in the fragment if this card and the 
//				next card have both q and a blank, also the postion is  set to the position minus one mdail 10-7-15
//v7.013.0047	If the new q or a were multi lined and while everything was still selected, you could see the q or a behind
//				the EditText as you are typing, so I am setting the q or a invisible to fix this.  I am still having 
//				trouble keeping the card on the answer after hitting save mdail 10-7-15
//v7.013.0046	The update work fine however when updating an answer after updating the fragment the card fliped back to the 
//				question so we need to flip it back to the answer mdail 10-7-15
//v7.013.0045	Fixed one last bug (I hope) in saving the Q's and A's mdail 10-7-15
//v7.013.0044	Fixed typo in the SQL strings, I had missed a space running two things together mdail 10-7-15
//v7.013.0043	Fixed the out of bounds error and am passing the correct index, also I split the update question/answer array
//				and database to two separate calls from the fragment (they were to methods, however I had it so you only needed to 
//				call one from the fragment, now you have to call both mdail 10-7-15
//v7.013.0042	I have been using the record id in place of the index, I have to figure out how to get the index mdail 10-6-15
//v7.013.0041	another try however I'm not sure why the save button didn't become visible mdail 10-6-15
//v7.013.0040	Getting closer yet another try to make it work properly mdail 10-6-15
//v7.013.0039	yet one more try to get the editing of Q's and A's working mdail 10-6-15
//v7.013.0038	Still working on getting the Q's and A's to update mdail 10-6-15
//v7.013.0037	I had the answer and questions backwards that is fixed mdail 10-6-15
//v7.013.0036	Fixed bug where edit button stayed on the screen however the edit view never displayed mdail 10-6-15
//v7.013.0035	Had to go back through the layouts because I forgot the start the buttons of invisible mdail 10-6-15
//v7.013.0034	Added 2 buttons and 2 editViews to the flash card fragment also added the code to edit and save the custom 
//				questions and answers mdail 10-6-15
//v7.012.0033	Fixed the favorite row to display the check box on the left like the rest of the row, the flash cards work with
//				the old code how ever there is now an entry at index 2 that is not being used as these cards have on less field 
//				I still have to figure out how to allow editing for chapter 20 and 21 though mdail 10-5-15
//v7.012.0032	Fixed trying to set a textView that is no longer on the fragment 10-5-15
//v7.012.0031	Fixed error is reading the list view and getting the chapters to show cards for mdail 10-5-15
//v7.012.0030	Fix index out of bounds on the arrays for the flash cards mdail 10-5-15
//v7.012.0029	Added the flash cards activity and the fragment for the flash cards 10-5-15
//v7.011.0028	Fixed a bug and also added the code the select all at startup mdail 10-5-15
//v7.011.0027	Fixed several error in the chapter select list, which let the page come up however the alignment
//				of the check boxes and chapters name pushed some off the screen, so I changed back to the way I did it in
//				the pdg program mdail 10-5-15
//v7.010.0026	Fix bug in select chapters activiy mdail 10-5-15
//v7.010.0025	I think I have the select activity ready to test to see if it opens without crashing, it still needs
//				work to be ready to move on to the next screen after that, I need to see what I missed changing from
//				the old to the new mdail 10-2-15
//v7.010.0024	Start working on the select cards activity getting it to load, fill the ListView and work with the
//				checkBoxes mdail 10-2-15
//v7.009.0023	Made some changes to mainy of the layouts to improve there appearance mdail 10-2-15
//v7.009.0022	Added getting the favorites ArrayList at startup and also a Boolean in the Chapters class named noFavorites
//				if it's true that means there are no cards marked as favorites, if it is false then there are some mdail 10-2-15
//v7.009.0021	Added the layouts for the Select Subject activity in all the sizes and portrait and landscape 
//				also added the ListView item layouts  mdail 10-2-15
//v7.008.0020	fixed the version number getting added 2 when the app first starts up mdail 10-1-15
//v7.008.0019	Added background="@null" to the image button so the background would be transparent also added a 
//				progress bar to the main activity layout, also the code to show and then hide it when the data is 
//				done loading mdail 10/1/15
//v7.008.0018	Had the table name and the database names reversed also added set database path method to run before 
//				checking and copying it into place mdail 10-1-15
//v7.008.0017	Fixed the database name being incorrect, and there for not getting copied into the database mdail 10-1-15
//v7.008.0016	Removed some invalid casts that are not needed in BM as they were in PDG as I changed the primary object
//				to Integers instead of long as they were last time mdail 10-1-15
//v7.008.0015	Added the AppRated, PrefsContract and ExceptionHandler classes to enable the App to request that the
//				user rates our app in 15 days or 15 runs of the app mdail 10-1-15
//v7.007.0014	Started to work on the MaiActivity adding the methods it will need to do what it needs to do mdail 10-1-15
//v7.006.0013	Changed the version number and version code to be after the last version's of the old Board Master mdail 10-1-15
//v2.006.0012	Added the Chapters class so the ArrayLists will stay open for the entire app mdail 9-30-15
//v2.005.0011	Added FlashCardsDataSource to get the records and put them into ArrayLists mdail 9-30-15
//v2.004.0010	Added the Cards class to hold the card data as it is read in from the database 9-30-15
//v2.003.0009 	Added the FlashCardsDBOpenHelper to help communicate with the database mdail 9-30-15
//v2.002.0008	Added the SqlStrings Data class to handle the SQL string needed for the app mdail 9-30-15
//v2.001.0007	Added the SharedProcedures module with minor modifications from the way it was in PDG mdail 9-30-15
//v2.000.0006	Removed 2 layouts that were for 240w, which eclipse says doesn't match anything and added sw600dp and
//				sw600dp-land so the 7" table and the 10" tablet used different layouts mdail 9-29-15
//v2.000.0005	Finished making all 8 layouts for the main screen
//v2.000.0004	Made images for different size screens made the drawables for the button (white/grey) and start making the main
//				page layouts mdail 9-29-15
//v2.000.0003	Made slight modification to the main app icon, setup file system for layouts, drawable so I can provide landscape and portrat
//				layouts and drawables mdail 9-22-15
//v2.000.0002	Made icon for all sizes, added the home icon to the main activity mdail 9-21-15
//v2.000.0001	Create this file for displaying the version number and entering notes as the project is built and updated mdail 9-21-15
//v2.000.0000 	Start the board master project mdail 9-21-15