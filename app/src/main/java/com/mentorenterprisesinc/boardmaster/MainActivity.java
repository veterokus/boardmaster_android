package com.mentorenterprisesinc.boardmaster;

import java.io.IOException;

import com.google.android.vending.licensing.AESObfuscator;
import com.google.android.vending.licensing.LicenseChecker;
import com.google.android.vending.licensing.LicenseCheckerCallback;
import com.google.android.vending.licensing.Policy;
import com.google.android.vending.licensing.ServerManagedPolicy;
import com.mentorenterprisesinc.boardmaster.adp_adrp.Adp_Adrp_Select_Chapters;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings.Secure;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends Activity {

	private static final String LOGTAG="MainActivity";
	public static final String PREFERENCES = "selected";
	private Context context;
	private FlashCardsDataSource datasource;
	private ProgressBar pb_main;
	private Boolean[] selected = new Boolean[23];
	private Boolean favSelected = false;
	/*
	 * 
	 * Below is for license checking with the google play store mdail 9-24-15
	 *  in on destroy above and oncreate there is some code for checking the license mdail 9-24-15
	 */
	private static final String BASE64_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAqavBy0hNS04t0msx5ueaU9sJqZDGJnC2XW9dOAO/JbsapIu5iybiJOwwnGYqdzquW/yQCPC2TwoleBDC5Hg1dxTZD466ipS2Yb9KQII0VVY2GS9L6MfjNkPAkpDRmEsEQhzfIOjZv1jJTTdaAaXVvhYnoZe7c/Of4sGPt8m5Doqp3CEM3VksdOngoUBcsVX8n2I3GjcOjUQRihc+WWXnksbA7cPLuAQeePYN97t1bAyTR8DXLRIIY9aMcye98U5pWfTQAPlCf68Ps9KBZG8CYZMOz2z1xjxFwBjW5bUEB/kxyHd41Sm3OAcUm989cb9PGAz6Rmw6jvbfUovdGV5UYQIDAQAB";

	// Generate your own 20 random bytes, and put them here.
	private static final byte[] SALT = new byte[] {
			(byte) 0xe3, (byte) 0x84, (byte) 0x9a, 0x51, 0x3e, (byte) 0xb1, (byte) 0xc2, 0x49, 0x31, 0x25, (byte) 0xf4, 0x5d, (byte) 0xcd, (byte) 0xe4, 0x79, 0x53,(byte) 0xbc, (byte) 0xd8, 0x66, (byte) 0xa0 
	};

	private TextView mStatusText;
	private Button mCheckLicenseButton;

	private LicenseCheckerCallback licenseCheckerCallback;
	private LicenseChecker checker;
	// A handler on the UI thread.
	private Handler mHandler;
	//End license checking with the google play store mdail 9-24-15
	@SuppressLint({ "NewApi" })
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
        Log.d(LOGTAG, "onCreate");
		getActionBar().setDisplayHomeAsUpEnabled(true);
		pb_main  = (ProgressBar) findViewById(R.id.pb_main);
		pb_main.setVisibility(View.VISIBLE);
        Log.d(LOGTAG, "onCreate pb_main = visible");
		this.context = this;			
		//below line is for license checking mdail 9-24-15	
		mStatusText = (TextView) findViewById(R.id.status_text);
        mCheckLicenseButton = (Button) findViewById(R.id.check_license_button);
        mCheckLicenseButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                doCheck();
            }
        });
        Log.d(LOGTAG, "onCreate before handler set");
		mHandler = new Handler();
        Log.d(LOGTAG, "onCreate after handler set");
		// Get the Phone Number of the phone to add security to the license
		String deviceId = Secure.getString(getContentResolver(), Secure.ANDROID_ID);
        Log.d(LOGTAG, "onCreate deviceId = " + deviceId);
        // Library calls this when it's done.
        if(deviceId == null || deviceId.isEmpty()){
            deviceId = getEmail();
        }
		// Library calls this when it's done.
		licenseCheckerCallback = new MyLicenseCheckerCallback();
		checker = new LicenseChecker(this, new ServerManagedPolicy(this,new AESObfuscator(SALT, getPackageName(),
					deviceId)),BASE64_PUBLIC_KEY);
		doCheck();
		mStatusText.setVisibility(View.INVISIBLE);
		mCheckLicenseButton.setVisibility(View.INVISIBLE);
        //end of code for license checking mdail 9-24-15
		//Sets the copyright line at the bottom to include the version number mdail 5-18-18
		TextView copyright = (TextView) findViewById(R.id.txtv_copyright);
		String copyrightText = copyright.getText().toString();
		copyright.setText("Version # " + VersionNumber.getVersionNumber(context) + " " + copyrightText);
		if(android.os.Build.VERSION.SDK_INT <= 22) {
			copyright.setTextColor(getResources().getColor(R.color.goldcolor));
		}else {
			copyright.setTextColor(getResources().getColor(R.color.goldcolor,null));
		}
		@SuppressWarnings("unused") // the line Chapters c = new Chapters() is use to intsatiate the class
		final Chapters GlobalChapters = new Chapters(context);
		FlashCardsDataSource fc = new FlashCardsDataSource(context);
		fc.open();
		Integer i = fc.getDBVersion();
		if(i.equals(1)) {
	        fc.close();
	        try{
                SharedProcedures.copyDataBase(context);
            }
            catch(IOException e){
                e.printStackTrace();
            }
		}
		fc.close();
		fc = null;
        Log.d(LOGTAG, "onCreate i = " + i);
		//Check top see if the data has already been loaded and if it has skip loading it again mdail 10-1-15
		if(Chapters.chapter21Total == null) {
			SharedProcedures.setDatabasePath();
			Boolean dbexists = false;
			dbexists = SharedProcedures.checkDatabasePath(context);
			if(!dbexists) {
				try {
					SharedProcedures.copyDataBase(context);
				}catch (IOException e){
					//if it throws an IOException, it display's an error toast message and stops, without the listeners set the
					//screen will not do anything as it shouldn't because something is very wrong mdail 10-1-15
					Toast toast = Toast.makeText(context, "copyDataBase Error Database Missing Contact Customer Support!",Toast.LENGTH_LONG );
					toast.show();
					return;
				}
			}
			datasource = new FlashCardsDataSource(context);
			//Start AsyncTask here which loads the data mdail 10-1-15
			new LoadData().execute();
		}else {
			setupListeners();
		}
		new AppRate(this).setShowIfAppHasCrashed(false).setMinDaysUntilPrompt(15).setMinLaunchesUntilPrompt(15).init();
	}
	// set the listeners for all the buttons and the switch on the screen mdail 10-1-15
	private void setupListeners(){
		Button adp_adrp =  (Button)findViewById(R.id.btn_adp);
		adp_adrp.setOnClickListener(
				new Button.OnClickListener() {
					@Override
					public void onClick(View v) {
						Intent intent = new Intent(MainActivity.this,Adp_Adrp_Select_Chapters.class);
						startActivity(intent);
					}
				});
		Button extras =  (Button)findViewById(R.id.btn_extras);
		extras.setOnClickListener(
				new Button.OnClickListener() {
					@Override
					public void onClick(View v) {
						Intent intent = new Intent(MainActivity.this,Select_Extras_Activity.class);
						startActivity(intent);
					}
				});
		Button subject =  (Button)findViewById(R.id.btn_subject);
		subject.setOnClickListener(
				new Button.OnClickListener() {
					@Override
					public void onClick(View v) {
						Intent intent = new Intent(MainActivity.this,Select_Chapters_Activity.class);
						startActivity(intent);
					}
				});	
		//After the last piece of code runs hide the progressBar mdail 10-1-15
		pb_main.setVisibility(View.INVISIBLE);		
	}
    
    @Override 
    public void onDestroy(){
        super.onDestroy();
        if(checker != null){
            checker.onDestroy();
        }
        Log.d(LOGTAG, "onDestroy:");
    }
	
	private void getSelected() {
		SharedPreferences prefs = getSharedPreferences(PREFERENCES,Context.MODE_PRIVATE);
		favSelected = prefs.getBoolean("favorite", false);
		if(favSelected) {
			return;
		}
		selected[0] = prefs.getBoolean(0 + "", false);
		if(selected[0]) {
			for(int i=0;i<23;i++) {
				selected[i] = true;
			}
			return;
		}
		for(int i=1;i<23;i++) {
			selected[i] = prefs.getBoolean(i + "", false);
		}
		Integer c = 0;
		for(int i=0;i<23;i++) {
			if(selected[i]) {
				c++;
			}
		}
		if(c.equals(0)) {
			SharedPreferences.Editor editor =  getSharedPreferences(PREFERENCES,Context.MODE_PRIVATE).edit();
			selected[0] = true;
			editor.putBoolean(0 + "", true);
			editor.commit();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		//code to exicute the back action from the icon mdail 9-21-15
		if (id == android.R.id.home) {
			finish();
		}
		return super.onOptionsItemSelected(item);
	}
	// Get the first email to add to the license LicenseChecker to make it more
    // secure mdail 9-24-15
    private String getEmail(){
        AccountManager manager = (AccountManager) getSystemService(ACCOUNT_SERVICE);
        Account[] list = manager.getAccounts();
        String email = new String();
        email = null;
        if(list[0] != null){
            email = list[0].name;
        }
        return email;
    }
	/*
	 * 
	 * Below is for license checking with the google play store mdail 9-24-15
	 *  in on destroy above and onCreate there is some code for checking the license mdail 9-24-15
	 */    
	protected Dialog onCreateDialog(int id) {
		final boolean bRetry = id == 1;
		return new AlertDialog.Builder(this)
				.setTitle(R.string.unlicensed_dialog_title)
				.setMessage(bRetry ? R.string.unlicensed_dialog_retry_body : R.string.unlicensed_dialog_body)
				.setPositiveButton(bRetry ? R.string.retry_button : R.string.buy_button, new DialogInterface.OnClickListener() {
					boolean mRetry = bRetry;
					public void onClick(DialogInterface dialog, int which) {
						if (mRetry) {
							doCheck();
						} else {
							Intent marketIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(
									"http://market.android.com/details?id=" + getPackageName()));
							startActivity(marketIntent);
							checker.onDestroy();
							finish();                        
						}
					}
				})
				.setNegativeButton(R.string.quit_button, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
					    checker.onDestroy();
						finish();
					}
				}).create();
	}

	private void doCheck() {
		Log.d(LOGTAG, "doCheck");
		 return; /* */
		//put above return statment in to bypass licensing mdail 10-1-15
		//checker.checkAccess(licenseCheckerCallback);
	}

	private void displayResult(final String result) {
		mHandler.post(new Runnable() {
			public void run() {
				//mStatusText.setText(result);
				//setProgressBarIndeterminateVisibility(false);
				//mCheckLicenseButton.setEnabled(true);
			}
		});
	}

	private void displayDialog(final boolean showRetry) {
		mHandler.post(new Runnable() {
			public void run() {
				//setProgressBarIndeterminateVisibility(false);
				showDialog(showRetry ? 1 : 0);
				//mCheckLicenseButton.setEnabled(true);
			}
		});
	}    

	private class MyLicenseCheckerCallback implements LicenseCheckerCallback {

		public void allow(int policyReason) {
			if (isFinishing()) {
				// Don't update UI if Activity is finishing.
				return;
			}
			// Should allow user access.
			displayResult(getString(R.string.allow));
		}

		public void dontAllow(int policyReason) {
			if (isFinishing()) {
				// Don't update UI if Activity is finishing.
				return;
			}
			displayResult(getString(R.string.dont_allow));
			// Should not allow access. In most cases, the app should assume
			// the user has access unless it encounters this. If it does,
			// the app should inform the user of their unlicensed ways
			// and then either shut down the app or limit the user to a
			// restricted set of features.
			// In this example, we show a dialog that takes the user to Market.
			// If the reason for the lack of license is that the service is
			// unavailable or there is another problem, we display a
			// retry button on the dialog and a different message.
			displayDialog(policyReason == Policy.RETRY);
		}

		public void applicationError(int errorCode) {
			if (isFinishing()) {
				// Don't update UI if Activity is finishing.
				return;
			}
			// This is a polite way of saying the developer made a mistake
			// while setting up or calling the license checker library.
			// Please examine the error code and fix the error.
			String result = String.format(getString(R.string.application_error), errorCode);
			Log.d(LOGTAG, "applicationError result = " + result);
			displayResult(result);
		}
	}
	//End of for license checking with the google play store mdail 9-24-15
	/*
	 * Subclass for the AsyncTask
	 * 
	 * Set async task to get the data in the background mdail 10-1-15
	 * 
	 * 																*/	  
	private class LoadData extends AsyncTask<String, Void, String> {
		//get the data for the questions and the scores
		@Override
		protected String doInBackground(String... params) {			
			datasource.findAll();
			return null;
		}

		@Override
		protected void onPreExecute() {					
		}

		@SuppressLint("NewApi")
		@Override
		protected void onPostExecute(String result) {
			//Calls the method that sets the click listeners for the 2 buttons on the page mdail 10-1-15
			setupListeners();
		}
		//Could be called if we wanted to make some kind of progress bar that showed the percent complete mdail 10-1-15
		@Override
		protected void onProgressUpdate(Void... values) {

		}
	}
}
