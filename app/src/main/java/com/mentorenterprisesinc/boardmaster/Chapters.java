package com.mentorenterprisesinc.boardmaster;

import java.util.ArrayList;
import java.util.List;

import android.app.Application;
import android.content.Context;
import android.util.Log;

public class Chapters extends Application {
    
    
	private static final String LOGTAG="Chapters";
	public static Integer[] chapterEndsAt = new Integer[24];
	public static Integer[] selectedChaptersForFlash = new Integer[24];
	//Hold all of informations that makes up questions for flash cards mdail 9-30-15
	public static List<Cards> chapter1 = new ArrayList<Cards>();
	public static List<Cards> chapter2 = new ArrayList<Cards>();
	public static List<Cards> chapter3 = new ArrayList<Cards>();
	public static List<Cards> chapter4 = new ArrayList<Cards>();
	public static List<Cards> chapter5 = new ArrayList<Cards>();
	public static List<Cards> chapter6 = new ArrayList<Cards>();
	public static List<Cards> chapter7 = new ArrayList<Cards>();
	public static List<Cards> chapter8 = new ArrayList<Cards>();
	public static List<Cards> chapter9 = new ArrayList<Cards>();
	public static List<Cards> chapter10 = new ArrayList<Cards>();
	public static List<Cards> chapter11 = new ArrayList<Cards>();
	public static List<Cards> chapter12 = new ArrayList<Cards>();
	public static List<Cards> chapter13 = new ArrayList<Cards>();
	public static List<Cards> chapter14 = new ArrayList<Cards>();
	public static List<Cards> chapter15 = new ArrayList<Cards>();
	public static List<Cards> chapter16 = new ArrayList<Cards>();
	public static List<Cards> chapter17 = new ArrayList<Cards>();
	public static List<Cards> chapter18 = new ArrayList<Cards>();
	public static List<Cards> chapter19 = new ArrayList<Cards>();
	public static List<Cards> chapter20 = new ArrayList<Cards>();
	public static List<Cards> chapter21 = new ArrayList<Cards>();
	public static List<Cards> chapter22 = new ArrayList<Cards>();
	public static List<Cards> favorites = new ArrayList<Cards>();
	//Holds the total number of questions selected mdail 9-30-15
	public static Integer totalQuestions = 0;
	//Holds the first and last chapters selected for flash cards mdail 9-30-15
	public static Integer firstSelectedChapter = 0;
	public static Integer lastSelectedChapter = 0;
	//Variable to hold the data set by the data returned from the raw query total number of questions by 
	//chapter and names mdail 10-1-15
	private static String chapter1Name;
	private static String chapter2Name;
	private static String chapter3Name;
	private static String chapter4Name;
	private static String chapter5Name;
	private static String chapter6Name;
	private static String chapter7Name;
	private static String chapter8Name;
	private static String chapter9Name;
	private static String chapter10Name;
	private static String chapter11Name;
	private static String chapter12Name;
	private static String chapter13Name;
	private static String chapter14Name;
	private static String chapter15Name;
	private static String chapter16Name;
	private static String chapter17Name;
	private static String chapter18Name;
	private static String chapter19Name;
	private static String chapter20Name;
	private static String chapter21Name;
	private static String chapter22Name;
	//Used to hold the chapter total number of questions
	private static Integer chapter1Total;
	private static Integer chapter2Total;
	private static Integer chapter3Total;
	private static Integer chapter4Total;
	private static Integer chapter5Total;
	private static Integer chapter6Total;
	private static Integer chapter7Total;
	private static Integer chapter8Total;
	private static Integer chapter9Total;
	private static Integer chapter10Total;
	private static Integer chapter11Total;
	private static Integer chapter12Total;
	private static Integer chapter13Total;
	private static Integer chapter14Total;
	private static Integer chapter15Total;
	private static Integer chapter16Total;
	private static Integer chapter17Total;
	private static Integer chapter18Total;
	private static Integer chapter19Total;
	private static Integer chapter20Total;
	//made this public so I could check to see if it was null in onCreate of MainActivity mdail 10-1-15
	public static Integer chapter21Total;
	private static Integer chapter22Total;
	//For getting and holding the context
	public static Context context;
	public static Boolean favorite = false;
	public static Boolean noFavorites = true;

	//main constructor, only needs to be called once at the start and the class is open as long as the app is, it
	//also sets the local context so it can be used thru out the class as needed mdail 9-30-15
	public Chapters(Context context1){
		context = context1;
	}
	//This was setting context however I moved it to the constructor mdail 10-1-15
	@Override
	public void onCreate()
	{
	}
	//Returns the favorite for the selected questions in the selected chapter mdail 10-1-15
	public static Integer getChapterFavorite(Integer index,Integer chapter) {
		Integer data;
		ArrayList<Cards> tempChapter; 
		tempChapter = selectChapter(chapter);
		data=tempChapter.get(index).getFavorite();
		tempChapter = null;
		return data;				
	}
	//Returns the Chapter # for the selected questions in the selected chapter mdail 10-1-15
	public static Integer getChapterChapter(Integer index,Integer chapter) {
		Integer data;
		ArrayList<Cards> tempChapter; 
		tempChapter = selectChapter(chapter);
		data=tempChapter.get(index).getChapter();
		tempChapter = null;
		return data;				
	}
	//Returns the id # for the selected questions in the selected chapter mdail 10-1-15
	public static Integer getChapterId(Integer index,Integer chapter) {
		Integer data;
		ArrayList<Cards> tempChapter; 
		tempChapter = selectChapter(chapter);
		data=tempChapter.get(index).getId();
		tempChapter = null;
		return data;				
	}
	//Returns the chapter name for the selected questions in the selected chapter mdail 10-1-15
	public static String getChapterChapter_name(Integer index,Integer chapter) {
		String data;
		ArrayList<Cards> tempChapter;  
		tempChapter = selectChapter(chapter);
		data=tempChapter.get(index).getChapter_name();
		tempChapter = null;
		return data;				
	}
	//Returns the question for the selected questions in the selected chapter mdail 10-1-15
	public static String getChapterQuestion(Integer index,Integer chapter) {
		String data;
		ArrayList<Cards> tempChapter;  
		tempChapter = selectChapter(chapter);
		data=tempChapter.get(index).getQuestion();
		tempChapter = null;
		return data;				
	}
	//Returns the answer for the selected questions in the selected chapter mdail 10-1-15
	public static String getChapterAnswer(Integer index,Integer chapter) {
		String data;
		ArrayList<Cards> tempChapter; 
		tempChapter = selectChapter(chapter);
		data=tempChapter.get(index).getAnswer();
		tempChapter = null;
		return data;				
	}
	//Updates the cards object and the database with the Question, note this is only allowed for chapter 21 for which it return
	//true it returns false if it is called for any chapter other than 21 and does nothing mdail 10-1-15
	public static Boolean setChapterQuestion(Integer index,Integer chapter,String data) {
		ArrayList<Cards> tempChapter; 
		tempChapter = selectChapter(chapter);
		tempChapter.get(index).setQuestion(data);
		return true;
	}
	//Updates the cards object and the database with the Answer, note this is only allowed for chapter for which it return
	//true it returns false if it is called for any chapter other than 21 and does nothing mdail 10-1-15
	public static Boolean setChapterAnswer(Integer index,Integer chapter,String data) {
		ArrayList<Cards> tempChapter;  
		tempChapter = selectChapter(chapter);
		tempChapter.get(index).setAnswer(data);
		tempChapter = null;
		return true;
	}
	public static void setChapterFavorite(Integer index,Integer chapter,Integer data) {
		ArrayList<Cards> tempChapter; 
		tempChapter = selectChapter(chapter);
		tempChapter.get(index).setFavorite(data);
		tempChapter = null;
		return;
	}
	//select the proper chapter to be able to return and set the proper array without having to copy the array mdail 9-30-15
	public static ArrayList<Cards> selectChapter(Integer chapterNumber){

		switch((int) chapterNumber){

		case 1:
			return (ArrayList<Cards>) chapter1;
		case 2:
			return (ArrayList<Cards>) chapter2;
		case 3:
			return (ArrayList<Cards>) chapter3;
		case 4:
			return (ArrayList<Cards>) chapter4;
		case 5:
			return (ArrayList<Cards>) chapter5;
		case 6:
			return (ArrayList<Cards>) chapter6;
		case 7:
			return (ArrayList<Cards>) chapter7;
		case 8:
			return (ArrayList<Cards>) chapter8;
		case 9:
			return (ArrayList<Cards>) chapter9;
		case 10:
			return (ArrayList<Cards>) chapter10;
		case 11:
			return (ArrayList<Cards>) chapter11;
		case 12:
			return (ArrayList<Cards>) chapter12;
		case 13:
			return (ArrayList<Cards>) chapter13;
		case 14:
			return (ArrayList<Cards>) chapter14;
		case 15:
			return (ArrayList<Cards>) chapter15;
		case 16:
			return (ArrayList<Cards>) chapter16;
		case 17:
			return (ArrayList<Cards>) chapter17;
		case 18:
			return (ArrayList<Cards>) chapter18;
		case 19:
			return (ArrayList<Cards>) chapter19;
		case 20:
			return (ArrayList<Cards>) chapter20;
		case 21:
			return (ArrayList<Cards>) chapter21;
		case 22:
			return (ArrayList<Cards>) chapter22;
		default:
			//It should never get here if it does there is a problem, however it is required by the compiler mdail 9-30-15
			return (ArrayList<Cards>) chapter1;
		}
	}
	//Set all the chapter array lists to the values for each chapter mdail 5-6-15
	public static Boolean setChapter1(List<Cards> newChapter){
		//add all the entries from the incoming list to the chapter1 list and return true is it works mdail 5-6-15
		return chapter1.addAll(newChapter);
	}

	public static Boolean setChapter2(List<Cards> newChapter){
		//add all the entries from the incoming list to the chapter1_01 list and return true is it works mdail 5-6-15
		return chapter2.addAll(newChapter);
	}

	public static Boolean setChapter3(List<Cards> newChapter){
		//add all the entries from the incoming list to the chapter1_02 list and return true is it works mdail 5-6-15
		return chapter3.addAll(newChapter);
	}

	public static Boolean setChapter4(List<Cards> newChapter){
		//add all the entries from the incoming list to the chapter1_03 list and return true is it works mdail 5-6-15
		return chapter4.addAll(newChapter);
	}
	public static Boolean setChapter5(List<Cards> newChapter){
		//add all the entries from the incoming list to the chapter2 list and return true is it works mdail 5-6-15
		return chapter5.addAll(newChapter);
	}

	public static Boolean setChapter6(List<Cards> newChapter){
		//add all the entries from the incoming list to the chapter3 list and return true is it works mdail 5-6-15
		return chapter6.addAll(newChapter);
	}

	public static Boolean setChapter7(List<Cards> newChapter){
		//add all the entries from the incoming list to the chapter3_05 list and return true is it works mdail 5-6-15
		return chapter7.addAll(newChapter);
	}

	public static Boolean setChapter8(List<Cards> newChapter){
		//add all the entries from the incoming list to the chapter3_07 list and return true is it works mdail 5-6-15
		return chapter8.addAll(newChapter);
	}

	public static Boolean setChapter9(List<Cards> newChapter){
		//add all the entries from the incoming list to the chapter3_09 list and return true is it works mdail 5-6-15
		return chapter9.addAll(newChapter);
	}

	public static Boolean setChapter10(List<Cards> newChapter){
		//add all the entries from the incoming list to the chapter3_28 list and return true is it works mdail 5-6-15
		return chapter10.addAll(newChapter);
	}

	public static Boolean setChapter11(List<Cards> newChapter){
		//add all the entries from the incoming list to the chapter3_37 list and return true is it works mdail 5-6-15
		return chapter11.addAll(newChapter);
	}

	public static Boolean setChapter12(List<Cards> newChapter){
		//add all the entries from the incoming list to the chapter3_9 list and return true is it works mdail 5-6-15
		return chapter12.addAll(newChapter);
	}

	public static Boolean setChapter13(List<Cards> newChapter){
		//add all the entries from the incoming list to the chapter4 list and return true is it works mdail 5-6-15
		return chapter13.addAll(newChapter);
	}

	public static Boolean setChapter14(List<Cards> newChapter){
		//add all the entries from the incoming list to the chapter5 list and return true is it works mdail 5-6-15
		return chapter14.addAll(newChapter);
	}
	public static Boolean setChapter15(List<Cards> newChapter){
		//add all the entries from the incoming list to the chapter6 list and return true is it works mdail 5-6-15
		return chapter15.addAll(newChapter);
	}

	public static Boolean setChapter16(List<Cards> newChapter){
		//add all the entries from the incoming list to the chapter6_22 list and return true is it works mdail 5-6-15
		return chapter16.addAll(newChapter);
	}

	public static Boolean setChapter17(List<Cards> newChapter){
		//add all the entries from the incoming list to the chapter7 list and return true is it works mdail 5-6-15
		return chapter17.addAll(newChapter);
	}

	public static Boolean setChapter18(List<Cards> newChapter){
		//add all the entries from the incoming list to the chapter18 list and return true is it works mdail 5-6-15
		return chapter18.addAll(newChapter);
	}

	public static Boolean setChapter19(List<Cards> newChapter){
		//add all the entries from the incoming list to the chapter19 list and return true is it works mdail 5-6-15
		return chapter19.addAll(newChapter);
	}

	public static Boolean setChapter20(List<Cards> newChapter){
		//add all the entries from the incoming list to the chapter20 list and return true is it works mdail 5-6-15 
		return chapter20.addAll(newChapter);
	}

	public static Boolean setChapter21(List<Cards> newChapter){
		//add all the entries from the incoming list to the chapter20 list and return true is it works mdail 5-6-15 
		return chapter21.addAll(newChapter);
	}

	public static Boolean setChapter22(List<Cards> newChapter){
		//add all the entries from the incoming list to the chapter20 list and return true is it works mdail 5-6-15 
		return chapter22.addAll(newChapter);
	}

	//set the name of the chapter by the chapter number 5-18-15
	public static String getChapterName(Integer chapterNumber){

		switch(chapterNumber){

		case 1:
			return chapter1Name;
		case 2:
			return chapter2Name;
		case 3:
			return chapter3Name;
		case 4:
			return chapter4Name;
		case 5:
			return chapter5Name;
		case 6:
			return chapter6Name;
		case 7:
			return chapter7Name;
		case 8:
			return chapter8Name;
		case 9:
			return chapter9Name;
		case 10:
			return chapter10Name;
		case 11:
			return chapter11Name;
		case 12:
			return chapter12Name;
		case 13:
			return chapter13Name;
		case 14:
			return chapter14Name;
		case 15:
			return chapter15Name;
		case 16:
			return chapter16Name;
		case 17:
			return chapter17Name;
		case 18:
			return chapter18Name;
		case 19:
			return chapter19Name;
		case 20:
			return chapter20Name;
		case 21:
			return chapter21Name;
		case 22:
			return chapter22Name;
		default:
			//It should never get here if it does there is a problem, however it is required by the compiler mdail 9-30-15
			return chapter22Name;
		}
	}
	//set the name of the chapter by the chapter number for the select chapter activity 9-30-15
	public static void setChapterName(Integer chapterNumber, String ChapterName){

		switch(chapterNumber){

		case 1:
			chapter1Name = ChapterName;
			break;
		case 2:
			chapter2Name = ChapterName;
			break;
		case 3:
			chapter3Name = ChapterName;
			break;
		case 4:
			chapter4Name = ChapterName;
			break;
		case 5:
			chapter5Name = ChapterName;
			break;
		case 6:
			chapter6Name = ChapterName;
			break;
		case 7:
			chapter7Name = ChapterName;
			break;
		case 8:
			chapter8Name = ChapterName;
			break;
		case 9:
			chapter9Name = ChapterName;
			break;
		case 10:
			chapter10Name = ChapterName;
			break;
		case 11:
			chapter11Name = ChapterName;
			break;
		case 12:
			chapter12Name = ChapterName;
			break;
		case 13:
			chapter13Name = ChapterName;
			break;
		case 14:
			chapter14Name = ChapterName;
			break;
		case 15:
			chapter15Name = ChapterName;
			break;
		case 16:
			chapter16Name = ChapterName;
			break;
		case 17:
			chapter17Name = ChapterName;
			break;
		case 18:
			chapter18Name = ChapterName;
			break;
		case 19:
			chapter19Name = ChapterName;
			break;
		case 20:
			chapter20Name = ChapterName;
			break;
		case 21:
			chapter21Name = ChapterName;
			break;
		case 22:
			chapter22Name = ChapterName;
			break;
		default:
			//It should never get here if it does there is a problem, however it is required by the compiler mdail 9-30-15
			chapter22Name = ChapterName;
		}
	}

	//set the total number of questions in the chapter by the chapter number 9-30-15
	public static void setChapterTotals(Integer chapterNumber, Integer total){

		switch(chapterNumber){

		case 1:
			chapter1Total = total;
			break;
		case 2:
			chapter2Total = total;
			break;
		case 3:
			chapter3Total = total;
			break;
		case 4:
			chapter4Total = total;
			break;
		case 5:
			chapter5Total = total;
			break;
		case 6:
			chapter6Total = total;
			break;
		case 7:
			chapter7Total = total;
			break;
		case 8:
			chapter8Total = total;
			break;
		case 9:
			chapter9Total = total;
			break;
		case 10:
			chapter10Total = total;
			break;
		case 11:
			chapter11Total = total;
			break;
		case 12:
			chapter12Total = total;
			break;
		case 13:
			chapter13Total = total;
			break;
		case 14:
			chapter14Total = total;
			break;
		case 15:
			chapter15Total = total;
			break;
		case 16:
			chapter16Total = total;
			break;
		case 17:
			chapter17Total = total;
			break;
		case 18:
			chapter18Total = total;
			break;
		case 19:
			chapter19Total = total;
			break;
		case 20:
			chapter20Total = total;
			break;
		case 21:
			chapter21Total = total;
			break;
		case 22:
			chapter22Total = total;
			break;
		default:
			//It should never get here if it does there is a problem, however it is required by the compiler mdail 9-30-15
			chapter22Total = total;
		}
	}
	//get the total number of questions in the chapter by the chapter number mdail 9-30-15
	public static Integer getChapterTotal(Integer chapterNumber){

		switch(chapterNumber){

		case 1:
			return chapter1Total;
		case 2:
			return chapter2Total;
		case 3:
			return chapter3Total;
		case 4:
			return chapter4Total;
		case 5:
			return chapter5Total;
		case 6:
			return chapter6Total;
		case 7:
			return chapter7Total;
		case 8:
			return chapter8Total;
		case 9:
			return chapter9Total;
		case 10:
			return chapter10Total;
		case 11:
			return chapter11Total;
		case 12:
			return chapter12Total;
		case 13:
			return chapter13Total;
		case 14:
			return chapter14Total;
		case 15:
			return chapter15Total;
		case 16:
			return chapter16Total;
		case 17:
			return chapter17Total;
		case 18:
			return chapter18Total;
		case 19:
			return chapter19Total;
		case 20:
			return chapter20Total;
		case 21:
			return chapter21Total;
		case 22:
			return chapter22Total;
		default:
			//It should never get here if it does there is a problem, however it is required by the compiler mdail 9-30-15
			return chapter22Total;
		}
	}

	public static void setTotalQuestions() {
		Integer i = 1;
		//looping thru until we get a zero or reach the end of the array will give use the location of one past the last 
		//chapter selected to show mdail 9-30-15
		while(Chapters.selectedChaptersForFlash[i] != 0 && i < 23) {
			i++;
		}
		//since the loop at the start should end up going past the end of selected chapter we need to -1 from i to find the 
		//last chapter mdail 9-30-15
		totalQuestions = Chapters.chapterEndsAt[i - 1];
		return;
	}
	//For when chapter 21 is selected, chapter 21's total question count only includes those that have been updated and are not 
	//blank, when a one is updated, we will add one more of the questions until all 30 have data in them, this will insure that
	//those that do have data will not have blank cards between then mdail 10-13-15
	public static void addOneToTotal() {
		Integer i = 1;
		//looping thru until we get a zero or reach the end of the array will give use the location of one past the last 
		//chapter selected to show mdail 9-30-15
		while(Chapters.chapterEndsAt[i] != 0 && i < 23) {
			i++;
			if(i.equals(23)) {
				break;
			}
		}
		Chapters.chapterEndsAt[i] = Chapters.chapterEndsAt[i] + 1;
		//since the loop at the start should end up going past the end of selected chapter we need to -1 from i to find the 
		//last chapter mdail 9-30-15
		Chapters.totalQuestions = Chapters.chapterEndsAt[i];
		return;
	}
	
	//Use to make sure all the possible entries in selectedChaptersForFlash are 0 to start with to make sure we do have any left
	//over values mdail 9-30-15
	public static void restSelectedChaptersForFlash(){
		for(int i=0; i<24; i++){
			selectedChaptersForFlash[i]=0;
		}
		return;
	}
	//Use to make sure all the possible entries in chapterEndsAt are 0 to start with to make sure we do have any 
	//left over values mdail 9-30-15
	public static void restChaptersEndsAt(){
		for(int i=0; i<24; i++){
			chapterEndsAt[i]=0;
		}
		return;
	}
}
