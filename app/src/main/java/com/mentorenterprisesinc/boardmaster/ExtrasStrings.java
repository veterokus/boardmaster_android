package com.mentorenterprisesinc.boardmaster;

public class ExtrasStrings {
	//This Class holds and returns the text that is displayed on the Extras_Activity mdail 10-16-17	
	public static String GetTitleText0() {
		return "Army Values";
	}
	public static String GetTitleText1() {
		return "Audie Murphy";
	}
	public static String GetTitleText2() {
		return "Example Bio 1";
	}
	public static String GetTitleText3() {
		return "Example Bio 2";
	}
	public static String GetTitleText4() {
		return "Sergeant Morales Club";
	}
	public static String GetTitleText5() {
		return "NCO Creed";
	}
	public static String GetTitleText6() {
		return "Soldier Creed";
	}
	public static String GetTitleText7() {
		return "Tips for the Boards";
	}
	public static String GetText0() {
		return "Loyalty – Bear true faith and allegiance to the U.S. Constitution, the Army, your Unit and other Soldiers.\n" +
				"Duty – Fulfill your obligations.\n" + "\n" + "Respect – Treat people as they should be treated.\n" + 
				"\n" + "Selfless Service – Put the welfare of the Nation, the Army, and your subordinates before your own.\n" + 
				"\n" + "Honor – Live up to all the Army Values.\n" + "\n" + "Integrity – Do what is right, legally and morally.\n" + 
				"\n" + "Personal Courage – Face fear, danger, or adversity (physical or moral).\n" + "\n" + 
				"LIVE THE ARMY VALUES\n";
	}	
	public static String GetText1() {
		return "    Audie Leon Murphy was a legend in his own time. A war hero, movie actor, writer of country and western " +
				"songs, and poet. His biography reads more like fiction than fact. He lived only 46 years, but he made a " +
				"lasting imprint on American history. Audie was born on a sharecropper's farm in North Texas on June 20, 1924. " +
				"As a boy, he chopped cotton for one dollar a day and was noted for his feats of derring-do and his accuracy " +
				"with a gun. He had only 5 years of schooling and was orphaned at age 16. After being refused enlistment " +
				"during World War II in both the Marines and Paratroopers for being too small (5'5\") and underweight " +
				"(110 lbs), he enlisted in the U.S. Army a few days after his 18th birthday.\n" + "\n" + "   After basic " +
				"training at Camp Wolters, Texas, and advanced training at Fort George G. Meade, " +
				"Maryland, Audie was sent overseas. He was assigned to the famous 15th Infantry Regiment of the 3rd Infantry " +
				"Division where he fought in North Africa, Sicily, Italy, France, and Germany. He earned a battlefields " +
				"commission for his courage and leadership ability as well as citations and decorations including every medal " +
				"for valor that America gives. He was also awarded three French and one Belgian medal. Lieutenant Audie Murphy " +
				"was the highest decorated Soldier in American history. \n" + "\n" + "    Discharged from the Army on September " +
				"21, 1945, Audie went to Hollywood at the invitation of movie star James Cagney. He remained in California for " +
				"the rest of his life and was closely associated with the movie industry, both as an actor and a producer. He " +
				"acted in 44 films, starring in 39 of them. His best known film was \"To Hell and Back,\" adopted from the best " +
				"selling book of his war experiences by the same name. Most of his movies were westerns. In 1955, Audie Murphy " +
				"was voted the Most Popular Western Actor in America by the Motion Picture Exhibitors. Audie wrote the lyrics " +
				"to 16 country and western songs, the most popular of which was \"Shutters and Boards,\" written with Scott " +
				"Turner in 1962. The song was recorded by over 30 pop singers, including Jerry Wallace, Dean Martin, and Porter " +
				"Waggoner. He was an accomplished poet; unfortunately, only a few of his poems have survived.\n" + "\n" + 
				"    In 1950 Audie joined the 36th Infantry Division (\"T-Patchers\") of the Texas National Guard and " +
				"served with it until 1966. He was a Mason and a Shriner and belonged to several veterans organizations. " +
				"Audie Murphy was killed in a plane crash on a mountain top near Roanoke, Virginia on May 28, 1971. " +
				"Fittingly, his body was recovered 2 days later on Memorial Day. Audie could very well be the last American " +
				"war hero. He was the greatest combat Soldier in the 200 plus year history of the United States.";
	}	
	public static String GetText2() {
		return "Good Morning, Good Afternoon or Good Evening,\n" + 
				"\n" + "    My name is SGT James Jones assigned to Aco 310 Fires Brigade. My home of record is Jacksonville, " +
				"FL. I entered the U.S. Army where I attended Basic Training at FT Jackson, SC 14-June-2008, AIT at FT Lee, VA " +
				"to achieve the MOS 91B Light Wheel Vehicle Mechanic. During my off-time, my hobbies are fishing and hunting. " +
				"My short-term Army goals are to win the NCO of the Month Board, attend SLC and go to Air Assault school. " +
				"Also I would like to complete my Bachelor’s Degree within the next 6 months. My long-term Army goals are to " +
				"be inducted into the prestigious SGT Audie Murphy Club, earn my Master’s Degree in Mechanical Engineer, " +
				"become a 1SG and to achieve the rank of Command Sergeant Major. This completes my biography at this time, " +
				"thank you.";
	}	
	public static String GetText3() {
		return "Good Morning, Good Afternoon or Good Evening,\n" + "\n" + "    I am SPC James Jones assigned to Aco " +
				"310 Fires Brigade. My home of record is Jacksonville, FL. I entered the U.S. Army where I attended Basic " +
				"Training at FT Jackson, SC 14-June-2008 and attended AIT at FT Lee, VA to achieve the MOS 91B Light Wheel " +
				"Vehicle Mechanic. I am married, 3 years to Kim M. Jones, we have 2 beautiful children Jenny R. Jones 3 " +
				"years old and James Jones Jr. 6 months old. During my off-time my hobbies are basketball and spending " +
				"time with my Family. My short-term Army goals are to win the Soldier of the Month Board, attend WLC and " +
				"go to Airborne school. Also I would like to complete my Associate’s Degree within the next 6 months. " +
				"My long-term Army goals are to be inducted into the prestigious SGT Audie Murphy Club, earn my Master’s " +
				"Degree in Mechanical Engineer, become a 1SG and to achieve the rank of Command Sergeant Major. This " +
				"completes my biography at this time, thank you.";
	}	
	public static String GetText4() {
		return "    The “Sergeant Morales Club” was established in 1973 by Lt. Gen. George S. Blanchard to promote the highest " +
				"ideals of integrity, professionalism and leadership for the enlisted force serving in Europe. The " +
				"organization embraces the same attributes as the U.S. Army’s \"Sergeant Audie Murphy Club.\" Membership " +
				"is exclusive and gained through a rigorous, competitive process.\n" + "\n" + "    SMC members exemplify " +
				"a special kind of leadership characterized by a personal concern for the needs, training, development and " +
				"welfare of Soldiers. SMC membership recognizes and rewards distinguished NCOs whose leadership achievements " +
				"merit special recognition and who have contributed significantly to developing a professional NCO Corps and " +
				"a combat-effective Army.";
	}
	public static String GetText5() {
		return "No one is more professional than I. I am a Noncommissioned Officer, a leader of soldiers. As a Noncommissioned " +
				"Officer, I realize that I am a member of a time honored corps, which is known as \"The Backbone of the Army\". " +
				"I am proud of the Corps of Noncommissioned Officers and will at all times conduct myself so as to bring credit " +
				"upon the Corps, the Military Service and my country regardless of the situation in which I find myself. I will " +
				"not use my grade or position to attain pleasure, profit, or personal safety.\n" + "\n" + "Competence is my " +
				"watchword. My two basic responsibilities will always be uppermost in my mind -- accomplishment of my mission " +
				"and the welfare of my soldiers. I will strive to remain technically and tactically proficient. I am aware of " +
				"my role as a Noncommissioned Officer. I will fulfill my responsibilities inherent in that role. All soldiers "  +
				"are entitled to outstanding leadership; I will provide that leadership. I know my soldiers and I will always " +
				"place their needs above my own. I will communicate consistently with my soldiers and never leave them " +
				"uninformed. I will be fair and impartial when recommending both rewards and punishment.\n" + "\n" + "Officers " +
				"of my unit will have maximum time to accomplish their duties; they will not have to accomplish mine. I will " +
				"earn their respect and confidence as well as that of my soldiers. I will be loyal to those with whom I serve; " +
				"seniors, peers, and subordinates alike. I will exercise initiative by taking appropriate action in the absence " +
				"of orders. I will not compromise my integrity, nor my moral courage. I will not forget, nor will I allow my " +
				"comrades to forget that we are professionals, Noncommissioned Officers, leaders!";
	}	
	public static String GetText6() {
		return "I am an American Soldier.\n" + "\n" + "I am a Warrior and a member of a team.\n" + "\n" + 
				"I serve the people of the United States, and live the Army values.\n" + "\n" + 
				"I will always place the mission first.\n" + "\n" + "I will never accept defeat.\n" + "\n" + 
				"I will never quit.\n" + "\n" + "I will never leave a fallen comrade.\n" + "\n" + 
				"I am disciplined, physically and mentally tough, trained and proficient in my warrior tasks and drills.\n" + 
				"\n" + "I always maintain my arms, my equipment, and myself.\n" + "\n" + "I am an expert and I am a " +
				"professional.\n" + "\n" + "I stand ready to deploy, engage, and destroy the enemies of the United " +
				"States of America in close combat.\n" + "\n" + "I am a guardian of freedom and the American way of life.\n" + 
				"\n" + "I am an American Soldier!";
	}	
	public static String GetText7() {
		return "1. Participate in as many mock Boards as possible. Soldier of the Month Boards are a great way to gain this " +
				"experience prior to the Promotion Board.\n" + "\n" + "2. Study and know the answers to the mock Board " +
				"selected questions this will allow you to concentrate on the Board procedures. Sitting at the position of " +
				"attention, question and answer dialog, Military Biography and executing sharp facing movements. A great tool " +
				"to build Board confidence!\n" + "\n" + "3. Appearance is important. Get a haircut before the Board if " +
				"applicable. Make sure you get your uniform cleaned. Have fellow Soldiers or NCO inspect your uniform before " +
				"the Board to make sure everything is in its proper place.\n" + "\n" + "4. Posture and Military bearing are " +
				"important. Sit straight up, your hands placed on your thighs. Keeping your Military bearing is an Art that " +
				"must be mastered. It’s alright for a Board Member to make jokes and laugh, but you must maintain your " +
				"Military bearing.\n" + "\n" + "5. Once told to take a seat, don’t hesitate to take a quick look behind you " +
				"and locate the chair.";
	}
}
