package com.mentorenterprisesinc.boardmaster;

import java.util.List;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.animation.Animator;
import android.annotation.SuppressLint;
import android.content.Context;


public class Flashcard_Fragment extends Fragment {
	
	private static final String LOGTAG="Flashcard_Fragment";
	public static final String ARG_SECTION_NUMBER = "1";
	//private static Integer lastPosition = 0;
	//private Integer nextPosition;
	private TextView tv_question;
	private TextView txtv_subject;
	private TextView tv_question_label;
	private TextView tv_total;
	private ImageView iv_question_card;
	private ImageView img_star;
	private TextView tv_answer;
	private ImageButton flip;
	private EditText edit_question;
	private EditText edit_answer;
	private String[] txtFields;
	public List<Cards> cardList;
	private Integer position;
	private Flashcard_Activity fa;
	public MenuItem saveCardData;
	public MenuItem editCardData;
	public MenuItem cancelEditData;
	public Menu editSaveMenu;
	private Boolean editCardDataVisible = false;
	private Boolean saveCardDataVisible = false;
	private boolean fragmentResume=false;
	private boolean fragmentVisible=false;
	private boolean fragmentOnCreated=false;
	private Boolean question = true;
	private InputMethodManager inputManager;
	public Animator currentAnimator;
	private int shortAnimationDuration;
	
	@SuppressLint("NewApi")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_flash, container, false);
		//get the screen number
		position = getArguments().getInt(ARG_SECTION_NUMBER);
		shortAnimationDuration = getResources().getInteger(android.R.integer.config_shortAnimTime);
		fa = (Flashcard_Activity)getActivity();
		if(!Chapters.favorite) {
			//when I decided to stop using nextPosition I fogot to change this back to position txtFields = findCurrentChapter(nextPosition); mdail 6-2-15
			txtFields = findCurrentChapter(position);
		}else {
			txtFields = findCurrentFavorite(position);
		}
		edit_question = (EditText) rootView.findViewById(R.id.edit_question);
		edit_answer = (EditText) rootView.findViewById(R.id.edit_answer);
		flip = (ImageButton) rootView.findViewById(R.id.img_flip_answer_scroll);
		//get the ids for the parts to change the visiblity on mdail 5-29-15
		tv_question = (TextView) rootView.findViewById(R.id.txtv_question_scroll);
		tv_question_label = (TextView) rootView.findViewById(R.id.txtv_fixedquestion_scrool);
		tv_answer = (TextView) rootView.findViewById(R.id.txtv_answer_scroll);
		tv_total = (TextView) rootView.findViewById(R.id.txtv_running_total);		
		iv_question_card = (ImageView) rootView.findViewById(R.id.img_flash_question_scroll);	
		img_star = (ImageView) rootView.findViewById(R.id.img_star);
		txtv_subject = (TextView) rootView.findViewById(R.id.txtv_subject);	
		//if there was no error set the text filed with the returned text mdail 6-3-15
		if(txtFields[0] != "Error") {
			tv_answer.setText(txtFields[0]);
			tv_question.setText(txtFields[1]);
			if(txtFields[4].equals("13")){
				txtv_subject.setText("Map Reading & Land Nav");
			}else if(txtFields[4].equals("4")){
				txtv_subject.setText("Military Customs & Courtesies");
			}else {
				txtv_subject.setText(txtFields[2]);
			}
			if(txtFields[4].equals("4") || txtFields[4].equals("13")) {
				tv_total.setText("S " + txtFields[4] + " - " + txtFields[3] + "/" + txtFields[5]);
			}else {
				tv_total.setText("Sbj " + txtFields[4] + " - " + txtFields[3] + "/" + txtFields[5]);
			}
			if(txtFields[4].equals("22")) {
				tv_question.setHint("Touch Edit, Then Enter Question Here and Touch Save");
			}
			if(txtFields[4].equals("21") || txtFields[4].equals("22")) {
				tv_answer.setHint("Touch Edit, Then Enter Answer Here and Touch Save");
			}
		}	
		//if Error was returned because we passed the end of the questions mdail 6-3-15
		if("Error".equals(txtFields[0])) {
			Toast toast = Toast.makeText(getActivity(), "Error getting next question!", Toast.LENGTH_SHORT);
			toast.show();
			return null;
		}
		//if Error was returned because we passed the end of the questions mdail 6-3-15
		if("End".equals(txtFields[0])) {
			Toast toast = Toast.makeText(getActivity(), "Tried to get question beyond the end of the selected limits!", Toast.LENGTH_SHORT);
			toast.show();
			return null;
		}
		if("1".equals(txtFields[8])) {
			img_star.setSelected(true);
		}else {
			img_star.setSelected(false);
		}
		/* need to figure out how to keep the scroll from go past the first blank question. It should show one blank
		 * however if the user tries to go past that one with out adding data to it it should not let them
		*/
		setupListeners();
		if(fa.cardDataUpdated == true) {
			tv_question.setVisibility(View.INVISIBLE);
			tv_question_label.setVisibility(View.INVISIBLE);
			iv_question_card.setVisibility(View.INVISIBLE);
			fa.cardDataUpdated = false;
		}
		if("22".equals(txtFields[4])) {
			editCardDataVisible = true;
		}else {
			editCardDataVisible = false;
		}
		return rootView;
	}
	// set the listeners for all the buttons and the switch on the screen mdail 10-7-15
	private void setupListeners(){
		//Set the click listener for the flip ImageButton to make the Question portion disappear or reappear as the case should be mdail 6-2-15
		flip.setOnClickListener(
				new Button.OnClickListener() {
					public void onClick(View v) {	
						if (tv_question.getVisibility() == View.VISIBLE || edit_question.getVisibility() == View.VISIBLE) {
							//Make the question parts invisible to reveal the answer mdail 6-1-15
							tv_question.setVisibility(View.INVISIBLE);
							tv_question_label.setVisibility(View.INVISIBLE);
							iv_question_card.setVisibility(View.INVISIBLE);
							question = false;
							if("21".equals(txtFields[4]) || "22".equals(txtFields[4])) {
								editCardDataVisible = true;
								saveCardDataVisible = false;
								if(saveCardData.isVisible()) {
									edit_question.setVisibility(View.INVISIBLE);
									edit_answer.setVisibility(View.INVISIBLE);
								}
								onPrepareOptionsMenu(editSaveMenu);
							}
						} else {
							//Make the question parts visible to reveal the question mdail 6-1-15
							tv_question.setVisibility(View.VISIBLE);
							tv_question_label.setVisibility(View.VISIBLE);
							iv_question_card.setVisibility(View.VISIBLE);
							question = true;
							if("22".equals(txtFields[4])) {
								editCardDataVisible = true;
								saveCardDataVisible = false;
								if(saveCardData.isVisible()) {
									edit_question.setVisibility(View.INVISIBLE);
									edit_answer.setVisibility(View.INVISIBLE);
								}
								onPrepareOptionsMenu(editSaveMenu);
							}
							if("21".equals(txtFields[4])) {
								editCardDataVisible = false;
								saveCardDataVisible = false;
								if(saveCardData.isVisible()) {
									edit_question.setVisibility(View.INVISIBLE);
									edit_answer.setVisibility(View.INVISIBLE);
								}
								onPrepareOptionsMenu(editSaveMenu);
							}
						}
					}
				});
		//Set the click listener for the flip ImageButton to make the Question portion disappear or reappear as the case should be mdail 6-2-15
		img_star.setOnClickListener(
				new ImageView.OnClickListener() {
					public void onClick(View v) {
		            	if (img_star.isSelected()) {
		    				//If the favorites is selected set it to not selected and update the database mdail 7-27-15
		            		img_star.setSelected(false);
		            		txtFields[8] = "0";
		            		FlashCardsDataSource ds = new FlashCardsDataSource(getActivity().getApplicationContext());
		            		ds.resetFavorite(Integer.parseInt(txtFields[7]));
			            	ds=null;
		            		if(!Chapters.favorite) {
		            			Integer  index = Integer.parseInt(txtFields[3]);
		            			index--;
		            			Integer chapter = Integer.parseInt(txtFields[4]);
		            			Chapters.setChapterFavorite(index ,chapter,0);
								txtFields[8] = Chapters.getChapterFavorite(index,chapter) + "";
								UpdateableFragment uf;
								uf = (UpdateableFragment)getActivity();
			            		uf.UpdateFlashList();
		            		}else {
		            			Chapters.favorites.get(Integer.parseInt(txtFields[3]) - 1).setFavorite(0);
		            			txtFields[8] = Chapters.favorites.get((Integer.parseInt(txtFields[3]) - 1)).getFavorite() + "";
		            			UpdateableFragment uf;
		            			uf = (UpdateableFragment)getActivity();
			            		uf.UpdateFlashList();
		            		}
		            	} else {
		            		img_star.setSelected(true);
		    				//If the favorites is not selected set it to selected and update the database mdail 7-27-15
		            		txtFields[8] = "1";
		            		FlashCardsDataSource ds = new FlashCardsDataSource(getActivity().getApplicationContext());
		            		ds.setFavorite(Integer.parseInt(txtFields[7]));
			            	ds=null;
		            		if(!Chapters.favorite) {
		            			Integer  index = Integer.parseInt(txtFields[3]);
		            			index--;
		            			Integer chapter = Integer.parseInt(txtFields[4]); 
		            			Chapters.setChapterFavorite(index ,chapter,1);
								txtFields[8] = Chapters.getChapterFavorite(index,chapter) + "";
								UpdateableFragment uf;
								uf = (UpdateableFragment)getActivity();
			            		uf.UpdateFlashList();
		            		}else {
		            			Chapters.favorites.get(Integer.parseInt(txtFields[3]) - 1).setFavorite(1);
		            			txtFields[8] = Chapters.favorites.get((int) (Integer.parseInt(txtFields[3]) - 1)).getFavorite() + "";
		            			UpdateableFragment uf;
		            			uf = (UpdateableFragment)getActivity();
			            		uf.UpdateFlashList();
		            		}
		            	}
					}
				});
	}
	//This function handles getting the data for the card if the favorites was chosen mdail 7-27-15
	private String[] findCurrentFavorite(Integer position) {
		Integer p = 0;
		p = position;
		p++;	
		String[] txtForFlash = new String[9]; 
		if(position >= Chapters.favorites.size()) {
			txtForFlash[0]="Error";
			return txtForFlash;		
		}
		txtForFlash[0] = Chapters.favorites.get(position).getAnswer();
		txtForFlash[1] = Chapters.favorites.get(position).getQuestion();
		txtForFlash[2] = Chapters.favorites.get(position).getChapter_name();
		txtForFlash[3] = p + ""; 
		txtForFlash[4] = Integer.toString(Chapters.favorites.get(position).getChapter()) + "";
		txtForFlash[5] = Chapters.favorites.size() + ""; 
		txtForFlash[7] = Integer.toString(Chapters.favorites.get(position).getId()) + ""; 
		txtForFlash[8] = Integer.toString(Chapters.favorites.get(position).getFavorite()) + "";
		return txtForFlash;
	}
	//This function calculates the the current chapter and question using the Chapters.chapterEndsAt & Chapters.selectedChaptersForFlash arrays
	//it the gets the data for the flash cards and returns array of string that hold the data to be put on the flash cards in txtForFlash
	//txtForFlash[0] is the answer text,  txtForFlash[1] is the question text,txtForFlash[2] is the page ref text,txtForFlash[3] is the question 
	//number converted to a string, txtForFlash[4] is the chapter number converted to text, txtForFlash[5] is the total question in the chapter
	//NOTE: both arrary data starts at 1 (0 is unused) to make it easier for humans to read mdail 6-1-15
	private String[] findCurrentChapter(Integer position) {
		Integer c = 1;
		Integer q = 0;
		Integer r = 1;
		String[] txtForFlash = new String[9]; 	
		//Moved here because this should find the current chapter location (change from <= to >= as the first would drop out on selected 
		//chapter 1 every time mdail 6-2-15
		while((position) >= Chapters.chapterEndsAt[r] && r < 22) {
			r++;
		}
		if(Chapters.selectedChaptersForFlash[r].equals(0)) {		
			txtForFlash[0]="Error";
			return txtForFlash;					
		}
		if(Chapters.totalQuestions + 1 <= position) {
			txtForFlash[0] = "End";
			return txtForFlash;
		}
		//This is the chapter number, r should be the index to the correct chapter mdail 6-1-15
		c = Chapters.selectedChaptersForFlash[r];
		if(c.equals(0)) {
			txtForFlash[0]="Error";
			return txtForFlash;		
		}
		//if we are index 1 we are in the first chapter selected so the position is the position in the chapters array mdail 6-2-15
		if(r.equals(1)) {
			q = position;
		}else {
			//changed from Chapters.selectedChaptersForFlash[r + 1] == 0 to Chapters.selectedChaptersForFlash[r] == 0 because we should not need to add one
			//to the index r to get the current question and if we do we would get the next chapters question and miss the last chapter all together mdail 6-2-155
			if(Chapters.selectedChaptersForFlash[r].equals(0)) {		
				txtForFlash[0]="Error";
				return txtForFlash;					
			}
			//Changed before it failed because r should be in the current chapter so by subtracting position from the chapter end at we should have the question
			//number within the chapter  q = position - Chapters.chapterEndsAt[c + 1]; 
			// changed again before it failed as r is the index not the chapter number mdail 6-2-15
			q = position - Chapters.chapterEndsAt[r - 1];
		}
		txtForFlash[0] = Chapters.getChapterAnswer(q, c);
		txtForFlash[1] = Chapters.getChapterQuestion(q, c);
		txtForFlash[2] = Chapters.getChapterChapter_name(q,c);
		Integer p = 0; 
		p = q + 1;
		txtForFlash[3] = p.toString();
		txtForFlash[4] = c.toString();
		txtForFlash[5] = Integer.toString(Chapters.getChapterTotal(c));	
		txtForFlash[7] = Integer.toString(Chapters.getChapterId(q, c));
		txtForFlash[8] = Integer.toString(Chapters.getChapterFavorite(q, c));
		return txtForFlash;
	}	
	@Override
	public void onPrepareOptionsMenu(Menu menu)
	{ 
		editSaveMenu = menu;
		editCardData = menu.findItem(R.id.btn_edit);
        saveCardData = menu.findItem(R.id.bnt_save);
        cancelEditData = menu.findItem(R.id.bnt_cancel);
	    if(editCardDataVisible) 
	    {           
	    	editCardData.setVisible(true);
	    	editCardData.setEnabled(true);
	    }
	    else
	    {
	    	editCardData.setEnabled(false);
	    	editCardData.setVisible(false);
	    }     
	    if(saveCardDataVisible) 
	    {           
	    	saveCardData.setVisible(true);
	    	saveCardData.setEnabled(true);         
	    	cancelEditData.setVisible(true);
	    	cancelEditData.setEnabled(true);
	    }
	    else
	    {
	    	saveCardData.setEnabled(false);
	    	saveCardData.setVisible(false);         
	    	cancelEditData.setVisible(false);
	    	cancelEditData.setEnabled(false);
	    }
	}
	//create the menu from the fragment mdail 10-8-15
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		editSaveMenu = menu;
		editCardData = menu.findItem(R.id.btn_edit);
        saveCardData = menu.findItem(R.id.bnt_save);
        cancelEditData = menu.findItem(R.id.bnt_cancel);
        onPrepareOptionsMenu(menu);
        
	}
	//create the menu functions from the fragment
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		switch (id) {
		case R.id.btn_edit:
			editData();
			return true;
		case R.id.bnt_save:
			saveData();
			return true;
        case R.id.bnt_cancel:
        	cancelEdit();
			return true;
        case R.id.bnt_help:
			return false;
        case android.R.id.home:
        	return false;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	public void cancelEdit() {
		if(!question) {
			tv_question.setVisibility(View.INVISIBLE);
			tv_question_label.setVisibility(View.INVISIBLE);
			iv_question_card.setVisibility(View.INVISIBLE);
			question = false;
			edit_answer.setText("");
			edit_answer.setVisibility(View.INVISIBLE);
			tv_answer.setVisibility(View.VISIBLE);
			saveCardDataVisible = false;
			if("22".equals(txtFields[4]) || "21".equals(txtFields[4])) {
				editCardDataVisible = true;
			}else {
				editCardDataVisible = false;
			}
			onPrepareOptionsMenu(editSaveMenu);
			inputManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
			inputManager.hideSoftInputFromWindow (getView().getWindowToken(),InputMethodManager.HIDE_NOT_ALWAYS);
		}else {
			edit_question.setText("");
			edit_question.setVisibility(View.INVISIBLE);
			tv_question.setVisibility(View.VISIBLE);
			tv_question_label.setVisibility(View.VISIBLE);
			iv_question_card.setVisibility(View.VISIBLE);
			question = true;
			tv_question.setVisibility(View.VISIBLE);
			saveCardDataVisible = false;
			if("22".equals(txtFields[4])) {
				editCardDataVisible = true;
			}else {
				editCardDataVisible = false;
			}
			onPrepareOptionsMenu(editSaveMenu);			
			inputManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
			inputManager.hideSoftInputFromWindow (getView().getWindowToken(),InputMethodManager.HIDE_NOT_ALWAYS);
		}
	}
	 
    @SuppressLint("NewApi")
	@SuppressWarnings("deprecation")
	public void editData() {
    	if(txtFields[4].equals("21")) {
			if(tv_question.getVisibility() == View.INVISIBLE){
				edit_answer.setVisibility(View.VISIBLE);
				String answerIn = tv_answer.getText().toString();
				if(TextUtils.isEmpty(answerIn)){
					edit_answer.setVisibility(View.VISIBLE);
					tv_answer.setVisibility(View.INVISIBLE);
					if(android.os.Build.VERSION.SDK_INT <= 22) {
						edit_answer.setTextColor(getResources().getColor(R.color.blackcolor));
					}else {
						edit_answer.setTextColor(getResources().getColor(R.color.blackcolor,null));
					}
					edit_answer.requestFocus();
					edit_answer.selectAll();
					saveCardDataVisible = true;
					editCardDataVisible = false;
					onPrepareOptionsMenu(editSaveMenu);		
				}else {
					edit_answer.setVisibility(View.VISIBLE);
					edit_answer.setText(tv_answer.getText());
					if(android.os.Build.VERSION.SDK_INT <= 22) {
						edit_answer.setTextColor(getResources().getColor(R.color.blackcolor));
					}else {
						edit_answer.setTextColor(getResources().getColor(R.color.blackcolor,null));
					}
					tv_answer.setVisibility(View.INVISIBLE);
					edit_answer.requestFocus();
					edit_answer.selectAll();
					saveCardDataVisible = true;
					editCardDataVisible = false;
					onPrepareOptionsMenu(editSaveMenu);		
				}
			}
		}
		if(txtFields[4].equals("22") && tv_question.getVisibility() == View.INVISIBLE) {
			edit_answer.setVisibility(View.VISIBLE);
			String answerIn = tv_answer.getText().toString();
			if(TextUtils.isEmpty(answerIn)){
				edit_answer.setVisibility(View.VISIBLE);
				tv_answer.setVisibility(View.INVISIBLE);
				if(android.os.Build.VERSION.SDK_INT <= 22) {
					edit_answer.setTextColor(getResources().getColor(R.color.blackcolor));
				}else {
					edit_answer.setTextColor(getResources().getColor(R.color.blackcolor,null));
				}
				edit_answer.requestFocus();
				edit_answer.selectAll();
				saveCardDataVisible = true;
				editCardDataVisible = false;
				onPrepareOptionsMenu(editSaveMenu);
			}else {
				edit_answer.setVisibility(View.VISIBLE);
				edit_answer.setText(tv_answer.getText());
				if(android.os.Build.VERSION.SDK_INT <= 22) {
					edit_answer.setTextColor(getResources().getColor(R.color.blackcolor));
				}else {
					edit_answer.setTextColor(getResources().getColor(R.color.blackcolor,null));
				}
				tv_answer.setVisibility(View.INVISIBLE);
				edit_answer.requestFocus();
				edit_answer.selectAll();
				saveCardDataVisible = true;
				editCardDataVisible = false;
				onPrepareOptionsMenu(editSaveMenu);
			}
		}
		if(txtFields[4].equals("22") && tv_question.getVisibility() == View.VISIBLE) {
			edit_question.setVisibility(View.VISIBLE);
			String questionIn = tv_question.getText().toString();
			if(TextUtils.isEmpty(questionIn)){
				edit_question.setVisibility(View.VISIBLE);
				tv_question.setVisibility(View.INVISIBLE);
				if(android.os.Build.VERSION.SDK_INT <= 22) {
					edit_question.setTextColor(getResources().getColor(R.color.blackcolor));
				}else {
					edit_question.setTextColor(getResources().getColor(R.color.blackcolor,null));
				}
				edit_question.requestFocus();
				edit_question.selectAll();
				saveCardDataVisible = true;
				editCardDataVisible = false;
				onPrepareOptionsMenu(editSaveMenu);
			}else {
				edit_question.setVisibility(View.VISIBLE);
				edit_question.setText(tv_question.getText());
				if(android.os.Build.VERSION.SDK_INT <= 22) {
					edit_question.setTextColor(getResources().getColor(R.color.blackcolor));
				}else {
					edit_question.setTextColor(getResources().getColor(R.color.blackcolor,null));
				}
				tv_question.setVisibility(View.INVISIBLE);
				edit_question.requestFocus();
				edit_question.selectAll();
				saveCardDataVisible = true;
				editCardDataVisible = false;
				onPrepareOptionsMenu(editSaveMenu);
			}								
		}
    }
    public void saveData() {
		if(!question) {
			String answerIn = edit_answer.getText().toString();
			if(!TextUtils.isEmpty(answerIn)){
				tv_answer.setVisibility(View.VISIBLE);
    			Integer  index = Integer.parseInt(txtFields[3]);
    			index--;
    			Integer chapter = Integer.parseInt(txtFields[4]); 
				Chapters.setChapterAnswer(index, chapter, edit_answer.getText().toString());
        		FlashCardsDataSource ds = new FlashCardsDataSource(getActivity().getApplicationContext());
        		ds.updateCustomAnswer(Integer.parseInt(txtFields[7]),edit_answer.getText().toString());
        		ds = null;
				txtFields[0] = edit_answer.getText().toString();
				tv_answer.setText(edit_answer.getText().toString());
				UpdateableFragment uf;
				uf = (UpdateableFragment)getActivity();
				uf.UpdateFlashList();
				edit_answer.setVisibility(View.INVISIBLE);
				tv_answer.setVisibility(View.VISIBLE);
				//After the update the card flips back to the question, so we need to keep it on the answer
				tv_question.setVisibility(View.INVISIBLE);
				tv_question_label.setVisibility(View.INVISIBLE);
				iv_question_card.setVisibility(View.INVISIBLE);
				saveCardDataVisible = false;
				editCardDataVisible = true;
				fa.cardDataUpdated = true;
				onPrepareOptionsMenu(editSaveMenu);
				inputManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
				inputManager.hideSoftInputFromWindow (getView().getWindowToken(),InputMethodManager.HIDE_NOT_ALWAYS);
			}else {
				Toast.makeText(getActivity(),"Please enter the text for your answer then press save again!",Toast.LENGTH_LONG).show();
			}
		}else{
			String questionIn = edit_question.getText().toString();
			if(!TextUtils.isEmpty(questionIn)){
				tv_question.setVisibility(View.VISIBLE);
				tv_question.setText(edit_question.getText().toString());
    			Integer  index = Integer.parseInt(txtFields[3]);
    			index--;
    			Integer chapter = Integer.parseInt(txtFields[4]); 
				Chapters.setChapterQuestion(index, chapter, edit_question.getText().toString());
        		FlashCardsDataSource ds = new FlashCardsDataSource(getActivity().getApplicationContext());
        		ds.updateCustomQuestion(Integer.parseInt(txtFields[7]),edit_question.getText().toString());
        		ds = null;
				txtFields[1] = edit_question.getText().toString();
				tv_question.setVisibility(View.VISIBLE);
				tv_question.setText(edit_question.getText().toString());
				edit_question.setVisibility(View.INVISIBLE);
				tv_question_label.setVisibility(View.VISIBLE);
				iv_question_card.setVisibility(View.VISIBLE);
				UpdateableFragment uf;
				uf = (UpdateableFragment)getActivity();
				uf.UpdateFlashList();
				saveCardDataVisible = false;
				editCardDataVisible = true;
				onPrepareOptionsMenu(editSaveMenu);
				inputManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
				inputManager.hideSoftInputFromWindow (getView().getWindowToken(),InputMethodManager.HIDE_NOT_ALWAYS);
			}else {
				Toast.makeText(getActivity(),"Please enter the text for your question then press save again!",Toast.LENGTH_LONG).show();
			}
		}
   	 
    }
	@Override
	public void onResume()
	{
		Log.i(LOGTAG, "onResume");
	    super.onResume();		
		if("22".equals(txtFields[4]) || "21".equals(txtFields[4])) {
			if(tv_question.getVisibility() == View.INVISIBLE) {
				editCardDataVisible = true;
			}
			if("22".equals(txtFields[4])) {
				editCardDataVisible = true;
			}
		}
		setHasOptionsMenu(true);
	}
	@Override
	public void onStart(){
		super.onStart();
		Log.i(LOGTAG, "onStart");
	}
	@Override 
	public void onPause() {
		super.onPause();
		Log.d(LOGTAG, "onPause:");
	}
	@Override 
	public void onStop() {
		super.onStop();
		Log.d(LOGTAG, "onStop:");
	}
	@Override 
	public void onDestroy() {
		super.onDestroy();
		Log.d(LOGTAG, "onDestroy:");
	}
	//When the card is swiped of the screen turn the invisible fields back to visible mdail 6-1-15
	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
		super.setUserVisibleHint(isVisibleToUser);
		// Make sure that we are currently visible
		if (isVisibleToUser && isResumed()){   // only at fragment screen is resumed
			fragmentResume=true;
	        fragmentVisible=false;
	        fragmentOnCreated=true;
	    }else  if (isVisibleToUser){        // only at fragment onCreated
	    	fragmentResume=false;
	        fragmentVisible=true;
	        fragmentOnCreated=true;
	    }
	    else if(!isVisibleToUser && fragmentOnCreated){// only when you go out of fragment screen
	    	fragmentVisible=false;
	        fragmentResume=false;
	    	//Make the question parts visible to reveal the answer mdail 5-29-15
			tv_question.setVisibility(View.VISIBLE);
			tv_question_label.setVisibility(View.VISIBLE);
			iv_question_card.setVisibility(View.VISIBLE);
			edit_question.setVisibility(View.INVISIBLE);
			edit_answer.setVisibility(View.INVISIBLE);
			if(saveCardDataVisible == true){
				editCardDataVisible = false;
				saveCardDataVisible = false;
				question = true;
				cancelEdit();
				saveCardData.setEnabled(false);
				saveCardData.setVisible(false); 
				editCardData.setEnabled(false); 
				editCardData.setVisible(false);
			}
			if(editCardDataVisible == true && ("21".equals(txtFields[4]) || "22".equals(txtFields[4]))){
				editCardDataVisible = false;
				question = true;
				cancelEdit();
				saveCardData.setEnabled(false);
				saveCardData.setVisible(false); 
				editCardData.setEnabled(false); 
				editCardData.setVisible(false);
			}
	    }
	}
}
