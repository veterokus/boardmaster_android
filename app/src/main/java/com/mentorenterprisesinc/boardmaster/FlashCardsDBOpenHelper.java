package com.mentorenterprisesinc.boardmaster;

import java.io.IOException;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

public class FlashCardsDBOpenHelper extends SQLiteOpenHelper {
	 
	private static final String LOGTAG = "FlashCardsDBOpenHelper";
	public static String DATABASE_PATH;
	public static final String DATABASE_NAME = "flash_cards.db";
	private static final int DATABASE_VERSION = 1;
	public static final String TABLE_CARDS = "cards";
	private Context context;
	//The field names for the Cards table mdail 9-30-15
    public static final String CARDS_FAVORITE = "favorite";
    public static final String CARDS_CHAPTER = "chapter";   
    public static final String CARDS_ID = "id";
    public static final String CARDS_CHAPTER_NAME = "chapter_name";
    public static final String CARDS_QUESTION = "question";
    public static final String CARDS_ANSWER = "answer";
	
	private static final String TABLE_CREATE_CARDS = 
					"CREATE TABLE cards (" + CARDS_FAVORITE + " NUMERIC, " +
					CARDS_CHAPTER + " NUMERIC, " + CARDS_ID + " INTEGER PRIMARY KEY, "
					+ CARDS_CHAPTER_NAME + " TEXT, " + CARDS_QUESTION + " TEXT, "
					+ CARDS_ANSWER + " TEXT);";

	public FlashCardsDBOpenHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
        SharedProcedures.setDatabasePath();
        this.context = context;
		Log.i(LOGTAG, "The path is " + DATABASE_PATH);
        DATABASE_PATH = context.getFilesDir().toString() + "/databases/";
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
    	boolean dbexists = SharedProcedures.checkDataBase();
    	if(!dbexists)
    	{
    		try 
    		{
    			SharedProcedures.copyDataBase(context);
    		} 
    		catch (IOException ioexception) 
    		{
    			Toast toast = Toast.makeText(context, "Error Trying to Copy the Database. A Blank Database was created, " +
    												  "Contact Customer Support!",Toast.LENGTH_LONG );
    			toast.show();
    			db.execSQL(TABLE_CREATE_CARDS);
    		}
    	}
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        boolean mDataBaseExist = SharedProcedures.checkDataBase();
        if(!mDataBaseExist)
        {
            //this.getWritableDatabase(); // was  getReadableDatabase();  mdail 4-7-15
            //this.close();
            try 
            {
                //Copy the database from assets mdail 4-7-15
            	SharedProcedures.copyDataBase(context);
            } 
            catch (IOException mIOException) 
            {
            	db.execSQL(TABLE_CREATE_CARDS);
				Toast toast = Toast.makeText(context, "Error Trying to Copy the Database. A Blank Database was " +
													  "created, Contact Customer Support!",Toast.LENGTH_LONG );
				toast.show();
            }
        }		
	}
}
