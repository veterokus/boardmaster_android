package com.mentorenterprisesinc.boardmaster;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

public class Help_Flash_Cards extends Activity {

	@SuppressLint({ "NewApi" })
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.help_flashcard_activity);
		//Set the Icon as a back button mdail 8-10-15
		getActionBar().setDisplayHomeAsUpEnabled(true);
		TextView txtv_edit_title = (TextView)findViewById(R.id.txtv_edit_title);
		if(android.os.Build.VERSION.SDK_INT <= 22) {
			txtv_edit_title.setTextColor(getResources().getColor(R.color.goldcolor));
		}else {
			txtv_edit_title.setTextColor(getResources().getColor(R.color.goldcolor,null));
		}
		TextView txtv_edit_body = (TextView)findViewById(R.id.txtv_edit_body);
		if(android.os.Build.VERSION.SDK_INT <= 22) {
			txtv_edit_body.setTextColor(getResources().getColor(R.color.goldcolor));
		}else {
			txtv_edit_body.setTextColor(getResources().getColor(R.color.goldcolor,null));
		}
		TextView txtv_save_title = (TextView)findViewById(R.id.txtv_save_title);
		if(android.os.Build.VERSION.SDK_INT <= 22) {
			txtv_save_title.setTextColor(getResources().getColor(R.color.goldcolor));
		}else {
			txtv_save_title.setTextColor(getResources().getColor(R.color.goldcolor,null));
		}
		TextView txtv_save_body = (TextView)findViewById(R.id.txtv_save_body);
		if(android.os.Build.VERSION.SDK_INT <= 22) {
			txtv_save_body.setTextColor(getResources().getColor(R.color.goldcolor));
		}else {
			txtv_save_body.setTextColor(getResources().getColor(R.color.goldcolor,null));
		}
		TextView txtv_quit_title = (TextView)findViewById(R.id.txtv_quit_title);
		if(android.os.Build.VERSION.SDK_INT <= 22) {
			txtv_quit_title.setTextColor(getResources().getColor(R.color.goldcolor));
		}else {
			txtv_quit_title.setTextColor(getResources().getColor(R.color.goldcolor,null));
		}
		TextView txtv_quit_body = (TextView)findViewById(R.id.txtv_quit_body);
		if(android.os.Build.VERSION.SDK_INT <= 22) {
			txtv_quit_body.setTextColor(getResources().getColor(R.color.goldcolor));
		}else {
			txtv_quit_body.setTextColor(getResources().getColor(R.color.goldcolor,null));
		}
		TextView txtv_flip_title = (TextView)findViewById(R.id.txtv_flip_title);
		if(android.os.Build.VERSION.SDK_INT <= 22) {
			txtv_flip_title.setTextColor(getResources().getColor(R.color.goldcolor));
		}else {
			txtv_flip_title.setTextColor(getResources().getColor(R.color.goldcolor,null));
		}
		TextView txtv_flip_body = (TextView)findViewById(R.id.txtv_flip_body);
		if(android.os.Build.VERSION.SDK_INT <= 22) {
			txtv_flip_body.setTextColor(getResources().getColor(R.color.goldcolor));
		}else {
			txtv_flip_body.setTextColor(getResources().getColor(R.color.goldcolor,null));
		}
		TextView txtv_arrows_title = (TextView)findViewById(R.id.txtv_arrows_title);
		if(android.os.Build.VERSION.SDK_INT <= 22) {
			txtv_arrows_title.setTextColor(getResources().getColor(R.color.goldcolor));
		}else {
			txtv_arrows_title.setTextColor(getResources().getColor(R.color.goldcolor,null));
		}
		TextView txtv_arrows_body = (TextView)findViewById(R.id.txtv_arrows_body);
		if(android.os.Build.VERSION.SDK_INT <= 22) {
			txtv_arrows_body.setTextColor(getResources().getColor(R.color.goldcolor));
		}else {
			txtv_arrows_body.setTextColor(getResources().getColor(R.color.goldcolor,null));
		}
		TextView txtv_arrows_title_q = (TextView)findViewById(R.id.txtv_arrows_title_q);
		if(android.os.Build.VERSION.SDK_INT <= 22) {
			txtv_arrows_title_q.setTextColor(getResources().getColor(R.color.goldcolor));
		}else {
			txtv_arrows_title_q.setTextColor(getResources().getColor(R.color.goldcolor,null));
		}
		TextView txtv_arrows_body_q = (TextView)findViewById(R.id.txtv_arrows_body_q);
		if(android.os.Build.VERSION.SDK_INT <= 22) {
			txtv_arrows_body_q.setTextColor(getResources().getColor(R.color.goldcolor));
		}else {
			txtv_arrows_body_q.setTextColor(getResources().getColor(R.color.goldcolor,null));
		}
		TextView txtv_die_title = (TextView)findViewById(R.id.txtv_die_title);
		if(android.os.Build.VERSION.SDK_INT <= 22) {
			txtv_die_title.setTextColor(getResources().getColor(R.color.goldcolor));
		}else {
			txtv_die_title.setTextColor(getResources().getColor(R.color.goldcolor,null));
		}
		TextView txtv_die_body = (TextView)findViewById(R.id.txtv_die_body);
		if(android.os.Build.VERSION.SDK_INT <= 22) {
			txtv_die_body.setTextColor(getResources().getColor(R.color.goldcolor));
		}else {
			txtv_die_body.setTextColor(getResources().getColor(R.color.goldcolor,null));
		}
		TextView txtv_star_title = (TextView)findViewById(R.id.txtv_star_title);
		if(android.os.Build.VERSION.SDK_INT <= 22) {
			txtv_star_title.setTextColor(getResources().getColor(R.color.goldcolor));
		}else {
			txtv_star_title.setTextColor(getResources().getColor(R.color.goldcolor,null));
		}
		TextView txtv_star_body = (TextView)findViewById(R.id.txtv_star_body);
		if(android.os.Build.VERSION.SDK_INT <= 22) {
			txtv_star_body.setTextColor(getResources().getColor(R.color.goldcolor));
		}else {
			txtv_star_body.setTextColor(getResources().getColor(R.color.goldcolor,null));
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		switch (id) {
		case android.R.id.home:
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	//goes back to the chapter list if the back button is pressed mdail 10-14-15
	@Override
	public void onBackPressed() {
		finish();
	}
}
