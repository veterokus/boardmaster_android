package com.mentorenterprisesinc.boardmaster;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

public class Select_Chapters_Activity extends Activity {

	private static final String LOGTAG="Select_Chapters_Activity";
	public static final String PREFERENCES = "selected";
	private CheckBox chk_favorites;
	private Parcelable state;
	private Context context1;
	private ListView chaptersListView;
	private ArrayAdapter<String>  adapter;
	private FlashCardsDataSource ds;
	public ProgressBar progress;
	private ProgressBar  pb;
	private Boolean[] selected = new Boolean[23];
	private Boolean favSelected = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.select_chapters_activity);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		progress = (ProgressBar)findViewById(R.id.pb_select_subject);
		progress.setVisibility(View.VISIBLE);
		this.context1 = this;
		ds = new FlashCardsDataSource(context1);
		ds.getFavoriteCards();
		ArrayList<String> chapterlist = new ArrayList<String>();
		chapterlist = buildList();
		chaptersListView = (ListView) findViewById(R.id.lstv_selec_chapters);
		chaptersListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);		
		adapter = new ArrayAdapter<String>(this,R.layout.modified_simple_list_item_multiple_choice,chapterlist);
		chaptersListView.setAdapter(adapter);
		adapter.notifyDataSetChanged();
		chk_favorites = (CheckBox) findViewById(R.id.chk_favorites);
		if(Chapters.noFavorites) {
			chk_favorites.setEnabled(false);
		}else {
			chk_favorites.setEnabled(true);
			chk_favorites.setChecked(false);
		}
		chapterlist = new ArrayList<String>();
		setViewListerners();
		getSelected();		
		progress = (ProgressBar)findViewById(R.id.pb_select_subject);
		progress.setVisibility(View.INVISIBLE);				
	}
	
	private void getSelected() {
		SharedPreferences prefs = getSharedPreferences(PREFERENCES,Context.MODE_PRIVATE);
		favSelected = prefs.getBoolean("favorite", false);
		if(favSelected) {
			chk_favorites.setChecked(true);
			return;
		}
		selected[0] = prefs.getBoolean(0 + "", false);
		if(selected[0]) {
			for(int i=0;i<23;i++) {
				selected[i] = true;
			}
			setAllSelected();
			return;
		}
		for(int i=1;i<23;i++) {
			selected[i] = prefs.getBoolean(i + "", false);
			chaptersListView.setItemChecked(i,selected[i]);
		}
	}
	
	private void setViewListerners() {
		chk_favorites = (CheckBox) findViewById(R.id.chk_favorites);
		OnClickListener fav_click = new OnClickListener() {
			//Make a listview onItemClicked listener that is called when Items are checked mdail 10-5-15
			@Override
			public void onClick(View v) {
				if(chk_favorites.isChecked()) {
					Chapters.favorite = true;
					favSelected = true;
					setAllNotSelected();
				}else {
					Chapters.favorite = false;
					setAllSelected();
				}
			}
		};
		//Set the onClick listener for the continue button so we can go on to the next screen when ready mdail 10-5-15
		chk_favorites.setOnClickListener(fav_click);   
		//Set on Item click listener for the ListView mdail 10-5-15
		chaptersListView = (ListView) findViewById(R.id.lstv_selec_chapters);
		OnItemClickListener itemClickListener = new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				if(position == 0){ 
					//if position 0 is clicked then we need to go though and make sure they are all selected mdail 10-2-15
					setReverseAllSelected();
					chk_favorites.setChecked(false);
					//if position isn't 0 the just toggle selected and background color mdail 10-2-15
				}else{
					//if the item does not have a check mark set the background to white and unchecked it mdail 10-2-15
					if(!chaptersListView.isItemChecked(position)){ 
						if(chaptersListView.isItemChecked(0)){
							chaptersListView.setItemChecked(0, false);
						}
					}else{ 
						//if 19 items are check, this would mean that all items except 0 are select, so set all items checked mdail 10-2-15
						if(chaptersListView.getCheckedItemCount() == 22){
							if(!chaptersListView.isItemChecked(0)){	
								chaptersListView.setItemChecked(0, true);
								chk_favorites.setChecked(false);
							}
						}
					}
				}
				//make sure the favorites and a chapter can not be chosen at the same time mdail 10-2-15
				if(chaptersListView.getCheckedItemCount() >= 1){
					chk_favorites.setChecked(false);			
				}
			}
		};
		chaptersListView.setOnItemClickListener(itemClickListener);
		Button button = (Button) findViewById(R.id.btn_start);
		//Set the onClick listener for the continue button so we can go on to the next screen when ready mdail 10-2-15
		button.setOnClickListener(new Button.OnClickListener() {
			//Make a listview onItemClicked listener that is called when Items are checked mdail 10-2-15
			@Override
			public void onClick(View v) {
				//Check whether we are doing favorites or picking from the list of chapters mdail 10-2-15
				if(!Chapters.favorite) {
					pb  = (ProgressBar) findViewById(R.id.pb_select_subject);
					pb.setVisibility(View.VISIBLE);
					//Get the list of selected chapters and send them to the flash cards so we know what to show  mdail 10-2-15
					//clear the selectedChaptersForFlash array to 0 to make sure it is blank and we don't get any unwanted values mdail 5-21-15
					Chapters.restSelectedChaptersForFlash();
					Chapters.restChaptersEndsAt();
					//chaptersListView = (ListView)findViewById(R.id.lstv_selec_chapters);
					Chapters.chapterEndsAt[1] = 0;
					Integer lC = 1;
					SharedPreferences.Editor editor =  getSharedPreferences(PREFERENCES,Context.MODE_PRIVATE).edit();
					//added to make sure the proper selection for the all selected gets saved mdail 10-19-15
					editor.putBoolean(0 + "", chaptersListView.isItemChecked(0));
					for(int i=1;i<23;i++){						
						if (chaptersListView.isItemChecked(i)){
							editor.putBoolean(i + "", true);
							//Add the chapter number to the list of selected chapters mdail 10-2-15
							Chapters.selectedChaptersForFlash[lC] = i;
							//Set the position of where new chapters start by getting Chapter totals
							if(lC.equals(1)){		        				
								Chapters.chapterEndsAt[lC] = Chapters.getChapterTotal(i);													
								//sets the chapter number of the first selected chapter mdail 10-2-15
								Chapters.firstSelectedChapter = i;
								lC++;
							}else{
								Chapters.chapterEndsAt[lC] = Chapters.chapterEndsAt[lC - 1] + Chapters.getChapterTotal(i);
								//sets the chapter number of the last selected chapter mdail 10-2-15
								Chapters.lastSelectedChapter = i;
								lC++;
							}
						}else {
							editor.putBoolean(i + "", false);
						}
					}
					editor.commit();
					Chapters.setTotalQuestions();
					if(Chapters.totalQuestions.equals(0)) {
						Toast toast = Toast.makeText(context1, 
								"Error no chapters selected! Please select at least one chapter before continueing!"
								, Toast.LENGTH_SHORT);
						toast.show();
						pb.setVisibility(View.INVISIBLE);
						return;
					}
					pb.setVisibility(View.INVISIBLE);
				}else {
					SharedPreferences.Editor editor =  getSharedPreferences(PREFERENCES,Context.MODE_PRIVATE).edit();
					editor.putBoolean("favorite", true);
					editor.commit();
					if(Chapters.noFavorites) {
						Toast toast = Toast.makeText(context1, "Error no favorites have been selected!", Toast.LENGTH_SHORT);
						toast.show();
						pb.setVisibility(View.INVISIBLE);
						return;	
					}
					if(Chapters.favorites == null) {
						Toast toast = Toast.makeText(context1,"Error no favorites have been selected!", Toast.LENGTH_SHORT);
						toast.show();
						pb.setVisibility(View.INVISIBLE);
						return;						
					}
					if(Chapters.favorites != null) {
						if(Chapters.favorites.size() == 0) {
							Toast toast = Toast.makeText(context1, "Error no favorites have been selected!", Toast.LENGTH_SHORT);
							toast.show();
							pb.setVisibility(View.INVISIBLE);
							return;	
						}
					}
				}
				//Move both the state = chaptersListView.onSaveInstanceState() & the chaptersListView.onRestoreInstanceState(state) to just
				//before starting the Flashcard_Activity because if done earlier it causes chaptersListView.getCheckedItemCount() 
				//not to work properly mdail 6-5-15
				//added to be able to save the state of the listView mdail 6-3-15
				state = chaptersListView.onSaveInstanceState();
				//added to be able to save the state of the listView mdail 6-3-15
				chaptersListView.onRestoreInstanceState(state);
				Intent intent = new Intent(Select_Chapters_Activity.this,Flashcard_Activity.class);
				startActivity(intent);
			}
		});
	}
	
	private void saveSelected() {
		SharedPreferences.Editor editor =  getSharedPreferences(PREFERENCES,Context.MODE_PRIVATE).edit();
		if(chk_favorites.isChecked()) {
			editor.putBoolean("favorite", true);
			editor.commit();
			return;
		}
		if(chaptersListView.isItemChecked(0)) {
			editor.putBoolean(0 + "", true);
			editor.commit();
			return;
		}
		for(int i=1;i<23;i++){				
			if (chaptersListView.isItemChecked(i)){
				editor.putBoolean(i + "", true);
			}else {
				editor.putBoolean(i + "", false);
			}
		}
		editor.commit();
	}
	
	@Override
	public void onResume()
	{
		Log.d(LOGTAG, "onResume:");
		super.onResume();
		ds = new FlashCardsDataSource(context1);
		ds.getFavoriteCards();
		getSelected();
	}
	@Override 
	public void onPause() {
		Log.d(LOGTAG, "onPause:");
		super.onPause();
		saveSelected();
	}

	//goes back to the chapter list if the back button is pressed mdail 10-2-15
    @Override
    public void onBackPressed() {
    	finish();
    }
	
	//Switch the whole list to check/not checked with the appropriate background color mdail 10-2-15
	private void setReverseAllSelected(){
		if(chaptersListView.isItemChecked(0)){
			chk_favorites.setChecked(false);
			for(int i = 0; i < 23; i++){
				chaptersListView.setItemChecked(i, true);
			}
		}else{
			for(int i = 0; i < 23; i++){
				chaptersListView.setItemChecked(i, false);
			}
		}
	}
	
	//Set the whole list as not selected with the Yellow background mdail 7-27-15
	private void setAllNotSelected(){
		//set the checked positions yellow and the unchecked white, only if the are visible mdail 7-27-15
		chaptersListView.setItemChecked(0, false);
		for(int i = 1; i < 23; i++){
			chaptersListView.setItemChecked(i, false);
		}
	}
	
	//Set the whole list as selected with the Yellow background mdail 5-21-15
	private void setAllSelected(){
		//set the checked positions yellow and the unchecked white, only if the are visible 6-10-15	
		chaptersListView.setItemChecked(0, true);
		chk_favorites.setChecked(false);
		for(int i = 1; i < 23; i++){
			chaptersListView.setItemChecked(i, true);
		}
	}
	// creates the list of chapters and chapter names mdail 10-2-15
	private static ArrayList<String> buildList(){
		ArrayList<String>  chapterListTemp = new ArrayList<String>();
		chapterListTemp.add("All");
		for(Integer i = 1; i<23; i++){
			chapterListTemp.add((i.toString()) + ": " + Chapters.getChapterName(i) + " - " + Chapters.getChapterTotal(i) + " Q's");
		}
		return chapterListTemp;
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		//code to exicute the back action from the icon mdail 10-2-15
		if (id == android.R.id.home) {
			Chapters.favorites = null;
			Chapters.favorite = false;
			finish();
		}
		return super.onOptionsItemSelected(item);
	}
}
