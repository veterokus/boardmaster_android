package com.mentorenterprisesinc.boardmaster;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.content.res.Resources;
import android.content.Context;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.widget.Toast;

@SuppressWarnings("unused")
public class SharedProcedures {
	
	//this class will hold proceudres and variables that can be called from anywhere mdail 4-9-15
	private static final String LOGTAG = "SharedProcedures";
    private static Context context;
    public static final Integer SQUARE = 3;
    public static final Integer LANDSCAPE = 2;
    public static final Integer PORTRAT = 1;
        
    public static Integer getOrientation(Context context)
    {
    	DisplayMetrics getOrient = context.getResources().getDisplayMetrics();
    	Integer orientation;
    	Integer dpWidth = Math.round(getOrient.widthPixels / getOrient.density);
    	Integer dpHeight = Math.round(getOrient.heightPixels / getOrient.density);
        if(dpWidth == dpHeight){
            orientation = SQUARE;
        }else{ 
            if(dpWidth < dpHeight){
                orientation = PORTRAT;
            }else { 
                 orientation = LANDSCAPE;
            }
        }
        return orientation;
    }
    
    public static Integer getScreenWidth(Context context) {
    	DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
    	Integer dpWidth = Math.round(displayMetrics.widthPixels / displayMetrics.density);
    	return dpWidth;
    }
    
    public static Integer getScreenHieght(Context context) {
    	DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
    	Integer dpHeight =  Math.round(displayMetrics.heightPixels / displayMetrics.density);
    	return dpHeight;
    }
    
    //set the path to the database mdail 4-9-15
    public static void setDatabasePath(){
    	FlashCardsDBOpenHelper.DATABASE_PATH = Chapters.context.getFilesDir().getPath() + "/databases/";
    	return;
    }
    //Check that the database exists 
    //here: /data/data/com.mentorenterprisesinc.leadershiphuntsville/databases/DATABASE_NAME mdail 4-7-15
    public static boolean checkDataBase()
    {
        File dbFile = new File(FlashCardsDBOpenHelper.DATABASE_PATH + FlashCardsDBOpenHelper.DATABASE_NAME);  
        return dbFile.exists();
    }

    //Checks to see if the database path exists mdail 4-9-15 and creates one if it doesn't mdail 4-9-15
    public static boolean checkDatabasePath(Context context){
    	File f = null;
    	f = new File(FlashCardsDBOpenHelper.DATABASE_PATH);
    	if (!f.exists() && !f.isDirectory()){	
    		f.mkdir();
    		f = new File(FlashCardsDBOpenHelper.DATABASE_PATH + FlashCardsDBOpenHelper.DATABASE_NAME);	
    		if (!f.exists()) {	
    			try{
    				copyDataBase(context);
    				return true;
    			}
    			catch (Exception e) {
    				Toast toast = Toast.makeText(context, "checkDatabasePath Error Trying to Copy the Database, " +
    						"Contact Customer Support!",Toast.LENGTH_LONG );
    				toast.show();
    				return false;
    			}
    		}
    	}
    	return true;
    }
    
    //Copy the database from assets mdail 4-7-15
    public static void copyDataBase(Context context) throws IOException
    {
    	boolean success = false;
    	success = checkDatabasePath(context);	
		if (!success){	
			success = makeDBdir(context);
			if (!success){
				Toast toast = Toast.makeText(context, "copyDataBase Error Database Missing Contact Customer Support!",Toast.LENGTH_LONG );
				toast.show();
				return;
			}
		};
        InputStream input = context.getAssets().open(FlashCardsDBOpenHelper.DATABASE_NAME);
        String outFileName = FlashCardsDBOpenHelper.DATABASE_PATH + FlashCardsDBOpenHelper.DATABASE_NAME;
        OutputStream output = new FileOutputStream(outFileName);
        byte[] bytebuffer = new byte[1024];
        int length;
        while ((length = input.read(bytebuffer))>0)
        {
            output.write(bytebuffer, 0, length);
        }
        output.flush();
        output.close();
        input.close();
    }
    
    public static String getFQPToDatabase(Context context){
    	return FlashCardsDBOpenHelper.DATABASE_PATH + FlashCardsDBOpenHelper.DATABASE_NAME;
    }
    
    public static boolean makeDBdir(Context context){
    	boolean success=false;
    	File f = null;
		f = new File(FlashCardsDBOpenHelper.DATABASE_PATH);
		success = f.mkdir();
		if(!success){
			Toast toast = Toast.makeText(context, "Error Database Directory is Missing, Contact Customer Support!",Toast.LENGTH_LONG );
			toast.show();
			return false;
		}
		return true;
    }
}
