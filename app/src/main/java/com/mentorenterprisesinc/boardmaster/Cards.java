package com.mentorenterprisesinc.boardmaster;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

public class Cards implements Parcelable {

	private static final String LOGTAG="Cards";
	public Context context;
	//to hold the fields from the cards table mdail 9-30-15
	private Integer favorite;
	private Integer chapter;
	private Integer id;
	private String chapter_name;
	private String question;
	private String answer;

	// main connector
	public Cards() {
		//empty constructor so it can initialize the class to use mdail 9-30-15
	}
	// constructor that allows passing to context so the shared preferences will work mdail 9-30-15
	public Cards(Context context) { 	
		this.context = context;
	}

	public Integer getFavorite() {
		return favorite;
	}
	public void setFavorite(Integer favorite) {
		this.favorite = favorite;
	}
	public Integer getChapter() {
		return chapter;
	}
	public void setChapter(Integer chapter) {
		this.chapter = chapter;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getChapter_name() {
		return chapter_name;
	}
	public void setChapter_name(String chapter_name) {
		this.chapter_name = chapter_name;
	}
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	public String getAnswer() {
		return answer;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	//required so the data can be passed to other Activities mdail 9-30-15
	public static final Parcelable.Creator<Cards> CREATOR =
			new Parcelable.Creator<Cards>() {

		//required override mdail 5-5-15
		@Override
		public Cards createFromParcel(Parcel source) {
			//DEBUGING Comment out prior to release mdail 9-30-15
			Log.i(LOGTAG, "createFromParcel");
			return new Cards(source);
		}
		//required override mdail 5-5-15
		@Override
		public Cards[] newArray(int size) {
			//DEBUGING Comment out prior to release mdail 9-30-15
			Log.i(LOGTAG, "newArray");
			return new Cards[size];
		}
	};
	//below is for saving the parceled data mdail 9-30-15
	public Cards(Parcel in) {
		//DEBUGING Comment out prior to release mdail 9-30-15
		Log.i(LOGTAG, "Parcel constructor");

		setFavorite(in.readInt());
		setId(in.readInt());
		setChapter(in.readInt());
		setChapter_name(in.readString());
		setQuestion(in.readString());
		setAnswer(in.readString());
	}
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		Log.i(LOGTAG, "writeToParcel");

		dest.writeInt(favorite);
		dest.writeInt(id);
		dest.writeInt(chapter);
		dest.writeString(chapter_name);
		dest.writeString(question);
		dest.writeString(answer);
	}
	@Override
	public int describeContents() {
		//DEBUGING Comment out prior to release mdail 9-30-15
		Log.i(LOGTAG, "Is describeContents ever called?");
		return 0;
	}
}