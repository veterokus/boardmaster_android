package com.mentorenterprisesinc.boardmaster;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class Select_Extras_Activity extends Activity {

	private static final String LOGTAG="Select_Extras_Activity";
	public final static String EXTRA_MESSAGE = "ExtraChoosen";
	private ListView lv;
	private ArrayAdapter<String> arrayAdapter;
	private Context context;
	private List<String> extrasList;
	private Integer screenWidth;
	private Integer screenHeight;
	private Integer screenSize;
	private Integer orientation;
	private TextView txtv_select_extras_title;
	private Integer setToScreenSize;
	public TextView extras_list_items;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.select_extras_activity);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		lv = (ListView) findViewById(R.id.lstv_select_extra);
		txtv_select_extras_title = (TextView) findViewById(R.id.txtv_select_extras_title);
		this.context = this;
		screenWidth = SharedProcedures.getScreenWidth(context);
		screenHeight = SharedProcedures.getScreenHieght(context);
		orientation = SharedProcedures.getOrientation(context);
		if(screenWidth < screenHeight) {
			screenSize = screenWidth;
		}else {
			screenSize = screenHeight;
		}
		if(screenSize <= 241) {
			if(orientation == SharedProcedures.SQUARE) {
				setToScreenSize = 0;
			}
			if(orientation == SharedProcedures.PORTRAT) {
				setToScreenSize = 0;
			}
			if(orientation == SharedProcedures.LANDSCAPE) {
				setToScreenSize = 1;
			}
    	}
    	if(screenSize > 241 && screenSize <= 321) {
			if(orientation == SharedProcedures.SQUARE) {
				setToScreenSize = 2;
			}
			if(orientation == SharedProcedures.PORTRAT) {
				setToScreenSize = 2;
			}
			if(orientation == SharedProcedures.LANDSCAPE) {
				setToScreenSize = 3;
			}
    	}
    	if(screenSize > 321 && screenSize <= 481) {
			if(orientation == SharedProcedures.SQUARE) {
				setToScreenSize = 4;
			}
			if(orientation == SharedProcedures.PORTRAT) {
				setToScreenSize = 4;
			}
			if(orientation == SharedProcedures.LANDSCAPE) {
				setToScreenSize = 5;
			}
    	}
    	if(screenSize > 481 && screenSize <= 601) {
			if(orientation == SharedProcedures.SQUARE) {
				setToScreenSize = 6;
			}
			if(orientation == SharedProcedures.PORTRAT) {
				setToScreenSize = 6;
			}
			if(orientation == SharedProcedures.LANDSCAPE) {
				setToScreenSize = 7;
			}
    	}
    	if(screenSize > 601) {
			if(orientation == SharedProcedures.SQUARE) {
				setToScreenSize = 8;
			}
			if(orientation == SharedProcedures.PORTRAT) {
				setToScreenSize = 8;
			}
			if(orientation == SharedProcedures.LANDSCAPE) {
				setToScreenSize = 9;
			}
    	}
		String[] extras = new String[8];
		extras[0] = "Army Values";
		extras[1] = "Audie Murphy";
		extras[2] = "Example Bio 1";
		extras[3] = "Example Bio 2";
		extras[4] = "Sergeant Morales Club";
		extras[5] = "NCO Creed";
		extras[6] = "Soldier Creed";
		extras[7] = "Tips for the Boards";
		extrasList = new ArrayList<String>(Arrays.asList(extras));
		arrayAdapter = new ExtraItemAdapter(this, R.layout.extras_list_item, extrasList);
		lv.setAdapter(arrayAdapter);
    	setScreenSizes(setToScreenSize);
		setViewListerners();
	}

	private void setViewListerners() {
		lv = (ListView) findViewById(R.id.lstv_select_extra);
		lv.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {				
				Intent intent = new Intent(context, Extras_Activity.class);
				switch(position) {
				case 0:
					intent.putExtra(EXTRA_MESSAGE, position + "");
					startActivity(intent);
					break;
				case 1:
					intent.putExtra(EXTRA_MESSAGE, position + "");
					startActivity(intent);
					break;
				case 2:
					intent.putExtra(EXTRA_MESSAGE, position + "");
					startActivity(intent);
					break;
				case 3:
					intent.putExtra(EXTRA_MESSAGE, position + "");
					startActivity(intent);
					break;
				case 4:
					intent.putExtra(EXTRA_MESSAGE, position + "");
					startActivity(intent);
					break;
				case 5:
					intent.putExtra(EXTRA_MESSAGE, position + "");
					startActivity(intent);
					break;
				case 6:
					intent.putExtra(EXTRA_MESSAGE, position + "");
					startActivity(intent);
					break;
				case 7:
					intent.putExtra(EXTRA_MESSAGE, position + "");
					startActivity(intent);
					break;
				default:
					break;
				}				
			}			
		});
	}
	
	private void setScreenSizes(Integer screenSizes) {
		lv = (ListView) findViewById(R.id.lstv_select_extra);
		MarginLayoutParams mlp = (MarginLayoutParams) lv.getLayoutParams();
		switch(screenSizes){
		case 0:
		case 1:
			txtv_select_extras_title.setTextSize(TypedValue.COMPLEX_UNIT_SP,20);
			lv.setDividerHeight(1);
			mlp.setMargins(10, 10, 10,10);
			break;
		case 2:
			txtv_select_extras_title.setTextSize(TypedValue.COMPLEX_UNIT_SP,24);
			lv.setDividerHeight(1);
			mlp.setMargins(15, 15, 15,15);
			break;
		case 3:
			txtv_select_extras_title.setTextSize(TypedValue.COMPLEX_UNIT_SP,24);
			lv.setDividerHeight(1);
			mlp.setMargins(30, 15, 30,15);
			break;
		case 4:
			txtv_select_extras_title.setTextSize(TypedValue.COMPLEX_UNIT_SP,32);
			lv.setDividerHeight(3);
			mlp.setMargins(20, 20, 20,20);
			break;
		case 5:
			txtv_select_extras_title.setTextSize(TypedValue.COMPLEX_UNIT_SP,32);
			lv.setDividerHeight(3);
			mlp.setMargins(40, 20, 40,20);
			break;
		case 6:
			txtv_select_extras_title.setTextSize(TypedValue.COMPLEX_UNIT_SP,37);
			lv.setDividerHeight(3);
			mlp.setMargins(40, 40, 40,40);
			break;
		case 7:
			txtv_select_extras_title.setTextSize(TypedValue.COMPLEX_UNIT_SP,37);
			lv.setDividerHeight(3);
			mlp.setMargins(80, 40, 80,40);
			break;
		case 8:
			txtv_select_extras_title.setTextSize(TypedValue.COMPLEX_UNIT_SP,44);
			lv.setDividerHeight(5);
			mlp.setMargins(60, 60, 60,60);
			break;
		case 9:
			txtv_select_extras_title.setTextSize(TypedValue.COMPLEX_UNIT_SP,44);
			lv.setDividerHeight(5);
			mlp.setMargins(80, 60, 80,60);
			break;
		default:
			break;
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		//code to exicute the back action from the icon mdail 10-2-15
		if (id == android.R.id.home) {
			Chapters.favorites = null;
			Chapters.favorite = false;
			finish();
		}
		return super.onOptionsItemSelected(item);
	}
	//goes back to the chapter list if the back button is pressed mdail 10-2-15
    @Override
    public void onBackPressed() {
    	finish();
    }
    
    //Adapter to make the list of extras mdail 10-15-15
	public class ExtraItemAdapter extends ArrayAdapter<String> {
  		
  		Context  context;
  		List<String> extrasTextIn;
  		
  		public ExtraItemAdapter(Context context, int resource, List<String> objects) {
  			super(context, resource, objects);
  			this.context = context;
  			extrasTextIn = objects;
  		}
  		@SuppressLint("InflateParams")
  		@Override
  		public View getView(int position, View convertView, ViewGroup parent) {
  			View rowView = convertView;
  			ViewHolder holder;
  			Integer positionInteger = Integer.valueOf(position + 1);
  			Integer even = Integer.valueOf(positionInteger % 2);
  			// reuse views 
  			if (rowView == null) {
  				LayoutInflater inflater = getLayoutInflater();
  				rowView = inflater.inflate(R.layout.extras_list_item, null);
  				// configure view holder
  				holder = new ViewHolder();
  				holder.extras_list_items = (TextView) rowView.findViewById(R.id.extras_list_items);
  				if (even.equals(0)) {
  					rowView.setBackgroundResource(R.drawable.listitem_even_selector);
  				} else {
  					rowView.setBackgroundResource(R.drawable.listitem_odd_selector); 
  				}
  				rowView.setTag(holder);
  			}else {
  				holder = (ViewHolder) rowView.getTag();
  			}
  			holder.extras_list_items.setText(extrasTextIn.get(position));
			setListItemText(setToScreenSize,holder);
  			if (even.equals(0)) {
  				rowView.setBackgroundResource(R.drawable.listitem_even_selector);
  			} else {
  				rowView.setBackgroundResource(R.drawable.listitem_odd_selector);  
  			}  
  			return rowView;
  		}
  		
  		private void setListItemText(Integer screenSizes, ViewHolder holderIn) {
  			switch(screenSizes){
  			case 0:
  			case 1:
  				holderIn.extras_list_items.setTextSize(TypedValue.COMPLEX_UNIT_SP,18);
  				break;
  			case 2:
  			case 3:
  				holderIn.extras_list_items.setTextSize(TypedValue.COMPLEX_UNIT_SP,20);
  				break;
  			case 4:
  			case 5:
  				holderIn.extras_list_items.setTextSize(TypedValue.COMPLEX_UNIT_SP,28);
  				break;
  			case 6:
  			case 7:
  				holderIn.extras_list_items.setTextSize(TypedValue.COMPLEX_UNIT_SP,33);
  				break;
  			case 8:
  			case 9:
  				holderIn.extras_list_items.setTextSize(TypedValue.COMPLEX_UNIT_SP,42);
  				break;
  			default:
  				break;
  			}
  		}
  	}
	
	//This will speed up the creation of the screen by not having to inflate the item view every time mdail 6-16-15
	static class ViewHolder {
		public TextView extras_list_items;
	}	
}
