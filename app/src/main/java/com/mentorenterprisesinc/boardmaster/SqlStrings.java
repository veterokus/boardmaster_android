package com.mentorenterprisesinc.boardmaster;
 
public class SqlStrings {

	//Makes a list of chapter number, chapter name and total records in the individual chapter
	public static String chaptersCounts() {
		return "Select chapter, chapter_name, count(chapter) from cards group by chapter;";
	}
	//Gets all data from the cards table
	public static String getAllCards() {
		return "Select * from cards order by chapter, id;";
	}
	//returns only the cards marked favorite mdail 9-30-15
	public static String getFavoriteCards() {
		return "Select * from cards where favorite > 0 order by chapter;";
	}
	//set the favorite field in the quiz database so when review quiz is selected we know the question the need to be reviewed
	public static String sqlSetFavorite(Integer id) {
		return "UPDATE cards SET favorite = 1 WHERE id = " + id + ";";
	}
	//set the favorite field in the quiz database so when review quiz is selected we know the question the need to be reviewed
	public static String sqlResetFavorite(Integer id) {
		return "UPDATE cards SET favorite = 0 WHERE id = " + id + ";";
	}
	//clear all favorite fields in the quiz database so when review quiz is selected we know the question the need to be reviewed
	public static String sqlClearFavorite() {
		return "UPDATE cards SET favorite = 0;";
	}
	//Add new Custom by updating blank records that are in the table already
	public static String sqlUpdateCustom(Integer id, String question, String answer) {
		return "UPDATE cards SET question = '" + question + "', answer = '" + answer + "' where id = " + id + " and chapter = 22;";
	}
	//Add new Custom by updating blank records that are in the table already
	public static String sqlUpdateCustomAnswer(Integer id, String answer) {
		return "UPDATE cards SET answer = '" + answer + "' where id = " + id + ";";
	}
	//Update Custom Question in the table already
	public static String sqlUpdateCustomQuestion(Integer id, String question) {
		return "UPDATE cards SET question = '" + question + "' where id = " + id + ";";
	}
	//Add new Custom by updating blank records that are in the table already
	public static String sqlAddCustomAnswer(String answer) {
		return "INSERT INTO cards (answer, chapter, chapter_name, favorite) VALUES ('" + answer + "', 22, 'Custom',0);";
	}
	//Add new Custom by updating blank records that are in the table already
	public static String sqlAddCustomQuestion(String question) {
		return "INSERT INTO cards (question, chapter, chapter_name, favorite) VALUES ('" + question + "', 22, 'Custom',0);";
	}
	//Add new Custom by updating blank records that are in the table already
	public static String sqlAddCustom(String question, String answer) {
		return "INSERT INTO cards (question, answer, chapter, chapter_name, favorite) VALUES ('" 
										+ question + "', '" + answer + "', 22, 'Custom',0);";
	}
}
