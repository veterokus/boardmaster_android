package com.mentorenterprisesinc.boardmaster;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.TextView;

public class Extras_Activity extends Activity {

	private static final String LOGTAG="Extras_Activity";
	private Integer extraToDisplay;
	private String getExtraToDisplay;
	private Context context;
	private Integer screenWidth;
	private Integer screenHeight;
	private Integer screenSize;
	private Integer orientation;
	private Integer setToScreenSize;
	private TextView txtv_extra;
	private TextView txtv_extra_title;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_extras);
		txtv_extra = (TextView) findViewById(R.id.txtv_extra);
		txtv_extra_title = (TextView) findViewById(R.id.txtv_extra_title);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		this.context = this;
		Intent intent = getIntent();
		getExtraToDisplay = intent.getStringExtra(Select_Extras_Activity.EXTRA_MESSAGE);
		Log.i(LOGTAG, "onCreate getExtraToDisplay = " + getExtraToDisplay );
		extraToDisplay = Integer.parseInt(getExtraToDisplay);
		GetScreen();
		GetExtra(extraToDisplay);
		setListItemAttributes();
	}

	private void GetExtra(int extra) {
		switch(extra) {
		case 0:
			txtv_extra_title.setText(ExtrasStrings.GetTitleText0());
			txtv_extra.setText(ExtrasStrings.GetText0());
			break;
		case 1:
			txtv_extra_title.setText(ExtrasStrings.GetTitleText1());
			txtv_extra.setText(ExtrasStrings.GetText1());
			break;
		case 2:
			txtv_extra_title.setText(ExtrasStrings.GetTitleText2());
			txtv_extra.setText(ExtrasStrings.GetText2());
			break;
		case 3:
			txtv_extra_title.setText(ExtrasStrings.GetTitleText3());
			txtv_extra.setText(ExtrasStrings.GetText3());
			break;
		case 4:
			txtv_extra_title.setText(ExtrasStrings.GetTitleText4());
			txtv_extra.setText(ExtrasStrings.GetText4());
			break;
		case 5:
			txtv_extra_title.setText(ExtrasStrings.GetTitleText5());
			txtv_extra.setText(ExtrasStrings.GetText5());
			break;
		case 6:
			txtv_extra_title.setText(ExtrasStrings.GetTitleText6());
			txtv_extra.setText(ExtrasStrings.GetText6());
			break;
		case 7:
			txtv_extra_title.setText(ExtrasStrings.GetTitleText7());
			txtv_extra.setText(ExtrasStrings.GetText7());
			break;
		default:
			break;			
		}
	}
	
	private void GetScreen() {

		screenWidth = SharedProcedures.getScreenWidth(context);
		screenHeight = SharedProcedures.getScreenHieght(context);
		orientation = SharedProcedures.getOrientation(context);
		if(screenWidth < screenHeight) {
			screenSize = screenWidth;
		}else {
			screenSize = screenHeight;
		}
		if(screenSize <= 241) {
			if(orientation == SharedProcedures.SQUARE) {
				setToScreenSize = 0;
			}
			if(orientation == SharedProcedures.PORTRAT) {
				setToScreenSize = 0;
			}
			if(orientation == SharedProcedures.LANDSCAPE) {
				setToScreenSize = 1;
			}
    	}
    	if(screenSize > 241 && screenSize <= 321) {
			if(orientation == SharedProcedures.SQUARE) {
				setToScreenSize = 2;
			}
			if(orientation == SharedProcedures.PORTRAT) {
				setToScreenSize = 2;
			}
			if(orientation == SharedProcedures.LANDSCAPE) {
				setToScreenSize = 3;
			}
    	}
    	if(screenSize > 321 && screenSize <= 481) {
			if(orientation == SharedProcedures.SQUARE) {
				setToScreenSize = 4;
			}
			if(orientation == SharedProcedures.PORTRAT) {
				setToScreenSize = 4;
			}
			if(orientation == SharedProcedures.LANDSCAPE) {
				setToScreenSize = 5;
			}
    	}
    	if(screenSize > 481 && screenSize <= 601) {
			if(orientation == SharedProcedures.SQUARE) {
				setToScreenSize = 6;
			}
			if(orientation == SharedProcedures.PORTRAT) {
				setToScreenSize = 6;
			}
			if(orientation == SharedProcedures.LANDSCAPE) {
				setToScreenSize = 7;
			}
    	}
    	if(screenSize > 601) {
			if(orientation == SharedProcedures.SQUARE) {
				setToScreenSize = 8;
			}
			if(orientation == SharedProcedures.PORTRAT) {
				setToScreenSize = 8;
			}
			if(orientation == SharedProcedures.LANDSCAPE) {
				setToScreenSize = 9;
			}
    	}    	
	}
	
	private void setListItemAttributes() {
		
		MarginLayoutParams mlpText = (MarginLayoutParams) txtv_extra.getLayoutParams();
		MarginLayoutParams mlpTitle = (MarginLayoutParams) txtv_extra_title.getLayoutParams();
			switch(setToScreenSize){
			case 0:
				txtv_extra_title.setTextSize(TypedValue.COMPLEX_UNIT_SP,14);
				txtv_extra.setTextSize(TypedValue.COMPLEX_UNIT_SP,12);
			//	mlp.setMargins(left, top, right, bottom);
				mlpTitle.setMargins(0, 10, 0, 10);
				mlpText.setMargins(20, 0, 20, 10);
				break;
			case 1:
				txtv_extra_title.setTextSize(TypedValue.COMPLEX_UNIT_SP,14);
				txtv_extra.setTextSize(TypedValue.COMPLEX_UNIT_SP,12);
				mlpTitle.setMargins(0, 10, 0, 10);
				mlpText.setMargins(20, 0, 20, 10);
				break;
			case 2:
				txtv_extra_title.setTextSize(TypedValue.COMPLEX_UNIT_SP,18);
				txtv_extra.setTextSize(TypedValue.COMPLEX_UNIT_SP,16);
				mlpTitle.setMargins(0, 15, 0, 15);
				mlpText.setMargins(25, 0, 25, 25);
				break;
			case 3:
				txtv_extra_title.setTextSize(TypedValue.COMPLEX_UNIT_SP,18);
				txtv_extra.setTextSize(TypedValue.COMPLEX_UNIT_SP,16);
				mlpTitle.setMargins(0, 10, 0, 10);
				mlpText.setMargins(20, 0, 20, 20);
				break;
			case 4:
				txtv_extra_title.setTextSize(TypedValue.COMPLEX_UNIT_SP,24);
				txtv_extra.setTextSize(TypedValue.COMPLEX_UNIT_SP,22);
				mlpTitle.setMargins(0, 20, 0, 20);
				mlpText.setMargins(30, 0, 30, 30);
				break;
			case 5:
				txtv_extra_title.setTextSize(TypedValue.COMPLEX_UNIT_SP,24);
				txtv_extra.setTextSize(TypedValue.COMPLEX_UNIT_SP,22);
				mlpTitle.setMargins(0, 10, 0, 10);
				mlpText.setMargins(30, 0, 30, 30);
				break;
			case 6:
				txtv_extra_title.setTextSize(TypedValue.COMPLEX_UNIT_SP,30);
				txtv_extra.setTextSize(TypedValue.COMPLEX_UNIT_SP,24);
				mlpTitle.setMargins(0, 25, 0, 25);
				mlpText.setMargins(35, 0, 35, 35);
				break;
			case 7:
				txtv_extra_title.setTextSize(TypedValue.COMPLEX_UNIT_SP,30);
				txtv_extra.setTextSize(TypedValue.COMPLEX_UNIT_SP,24);
				mlpTitle.setMargins(0, 10, 0, 10);
				mlpText.setMargins(35, 0, 35, 35);
				break;
			case 8:
				txtv_extra_title.setTextSize(TypedValue.COMPLEX_UNIT_SP,38);
				txtv_extra.setTextSize(TypedValue.COMPLEX_UNIT_SP,33);
				mlpTitle.setMargins(0, 30, 0, 30);
				mlpText.setMargins(35, 0, 35, 35);
				break;
			case 9:
				txtv_extra_title.setTextSize(TypedValue.COMPLEX_UNIT_SP,38);
				txtv_extra.setTextSize(TypedValue.COMPLEX_UNIT_SP,33);
				mlpTitle.setMargins(0, 10, 0, 10);
				mlpText.setMargins(35, 0, 35, 35);
				break;
			default:
				break;
			}
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		//code to exicute the back action from the icon mdail 10-2-15
		if (id == android.R.id.home) {
			Chapters.favorites = null;
			Chapters.favorite = false;
			finish();
		}
		return super.onOptionsItemSelected(item);
	}
	//goes back to the chapter list if the back button is pressed mdail 10-2-15
    @Override
    public void onBackPressed() {
    	finish();
    }
}
