package com.mentorenterprisesinc.boardmaster;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class FlashCardsDataSource extends Activity {

	public static final String LOGTAG="FlashCardsDataSource";
	private Context context;
	private Integer lastChapterWas = 1;
	private static SQLiteDatabase database;
	private FlashCardsDBOpenHelper dbhelper;
	private List<Cards> cards = null;
	public Cursor cursor;
	
	private final String[] allColumns = {
			FlashCardsDBOpenHelper.CARDS_FAVORITE,
			FlashCardsDBOpenHelper.CARDS_CHAPTER,
			FlashCardsDBOpenHelper.CARDS_ID,
			FlashCardsDBOpenHelper.CARDS_CHAPTER_NAME,
			FlashCardsDBOpenHelper.CARDS_QUESTION,
	};
	
	public FlashCardsDataSource(Context context) {  
		this.context = context;
		dbhelper = new FlashCardsDBOpenHelper(context); 
	}
	
	public void open() {
		//Check to see if the database file is there mdail 5-8-15
		Boolean dbExits = SharedProcedures.checkDataBase();
		if(!dbExits){
			try {
				//If it is not there copy it into place mdail 5-8-15
				SharedProcedures.copyDataBase(context);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		//open the database mdail 5-8-15
		database = SQLiteDatabase.openDatabase(SharedProcedures.getFQPToDatabase(context), null, SQLiteDatabase.OPEN_READWRITE);
	} 
	
	public void close() { 	
		//close the database mdail 5-8-15
		database.close();
	}
	//Sets the Favorite filed in the database mdail 7-27-15
	public Void setFavorite(Integer id) throws SQLException {
		open();//opens the db mdail 7-14-15
		database.execSQL(SqlStrings.sqlSetFavorite(id));
		close();
		return null;
	}
	//Resets the Favorite filed in the database mdail 7-27-15
	public Void resetFavorite(Integer id) throws SQLException {
		open();//opens the db mdail 7-14-15
		database.execSQL(SqlStrings.sqlResetFavorite(id));
		close();
		return null;
	}
	//Resets the Favorite filed in the database mdail 7-27-15
	public Void clearFavorite() throws SQLException {
		open();//opens the db mdail 7-14-15
		database.execSQL(SqlStrings.sqlClearFavorite());
		close();
		return null;
	}
	//Adds custom data to chapter 20 & 21 so the use can have custom cards mdail 7-27-15
	public Void updateCustomQuestion(Integer id, String question) throws SQLException {
		open();//opens the db mdail 7-14-15
		database.execSQL(SqlStrings.sqlUpdateCustomQuestion(id,question));
		close();
		return null;
	}
	//Updates custom data to chapter 21 so the use can have custom cards mdail 7-27-15
	public Void updateCustomAnswer(Integer id, String answer) throws SQLException {
		open();//opens the db mdail 7-14-15
		database.execSQL(SqlStrings.sqlUpdateCustomAnswer(id,answer));
		close();
		return null;
	}	
	//Updates custom data to chapter 21 so the use can have custom cards mdail 7-27-15
	public Void updateCustom(Integer id, String question, String answer) throws SQLException {
		open();//opens the db mdail 7-14-15
		database.execSQL(SqlStrings.sqlUpdateCustom(id,question,answer));
		close();
		return null;
	}
	//Adds custom data to chapter 21 so the use can have custom cards mdail 7-27-15
	public Void addCustomQuestion(String question) throws SQLException {
		open();//opens the db mdail 7-14-15
		database.execSQL(SqlStrings.sqlAddCustomQuestion(question));
		close();
		return null;
	} 
	//Adds custom data to chapter 21 so the use can have custom cards mdail 7-27-15
	public Void addCustomAnswer(String answer) throws SQLException {
		open();//opens the db mdail 7-14-15
		database.execSQL(SqlStrings.sqlAddCustomAnswer(answer));
		close();
		return null;
	}
	//Adds custom data to chapter 21 so the use can have custom cards mdail 7-27-15
	public Void addCustom(String question, String answer) throws SQLException {
		open();//opens the db mdail 7-14-15
		database.execSQL(SqlStrings.sqlAddCustom(question,answer));
		close();
		return null;
	}
	
	public void findAll() throws SQLException {
		open();//opens the db mdail 5-4-15
		// gets the records mdail 5-8-15
		cursor = database.rawQuery(SqlStrings.getAllCards(),null);
		if (cursor != null){
			//this is a trick from the days of working on windows to make sure the count would include all row and not only 
			//those loaded so far mdail 5-8-15
			cursor.moveToLast();
			cursor.moveToFirst();
			convertCursorToChapters(cursor);
		}
		cursor = database.rawQuery(SqlStrings.chaptersCounts(), null);
		convetCursorToChaptersInfo(cursor);
		cursor = database.rawQuery(SqlStrings.getFavoriteCards(), null);
		convertCursorToFavorite(cursor);
		//database close mdail 5-7-15;
		close();
		return;
	}
	
	public void getFavoriteCards() throws SQLException {
		open();
		cursor = database.rawQuery(SqlStrings.getFavoriteCards(), null);
		convertCursorToFavorite(cursor);
		//database close mdail 10-2-15;
		close();
	}
	
	//converts the Seen count to a list of the number of questions mastered mdail 6-18-15
	public Void convertCursorToFavorite(Cursor inCursor) {
		//Make sure we don't have any review questions left from the last review mdail 7-23-15
		Chapters.favorites = null;
		Chapters.favorites = new ArrayList<Cards>();
		// holds indiviual row data mdail 7-23-15
		Cards card;		
		if (inCursor.getCount() > 0) {
			inCursor.moveToFirst();
			Chapters.noFavorites = false;
			//moves through the cursor rows one at a time mdail 7-23-15
			while(!inCursor.isAfterLast()){
				//creates a new quiz object then fill the fields with the data from the cursor mdail 7-23-15
				card = new Cards();
				//check to make sure the fields that can be null are not null and set the field to the value else set it 
				//to "" for strings mdail 7-23-15
				card.setFavorite(inCursor.getInt(inCursor.getColumnIndexOrThrow(FlashCardsDBOpenHelper.CARDS_FAVORITE)));
				card.setChapter(inCursor.getInt(inCursor.getColumnIndexOrThrow(FlashCardsDBOpenHelper.CARDS_CHAPTER)));
				card.setId(inCursor.getInt(inCursor.getColumnIndexOrThrow(FlashCardsDBOpenHelper.CARDS_ID)));
				card.setChapter_name(inCursor.getString(inCursor.getColumnIndexOrThrow(FlashCardsDBOpenHelper.CARDS_CHAPTER_NAME)));
				if(!inCursor.isNull(inCursor.getColumnIndexOrThrow(FlashCardsDBOpenHelper.CARDS_QUESTION))){
					card.setQuestion(inCursor.getString(inCursor.getColumnIndexOrThrow(FlashCardsDBOpenHelper.CARDS_QUESTION)));
				}else{
					card.setQuestion("");
				}
				if(!inCursor.isNull(inCursor.getColumnIndexOrThrow(FlashCardsDBOpenHelper.CARDS_ANSWER))){
					card.setAnswer(inCursor.getString(inCursor.getColumnIndexOrThrow(FlashCardsDBOpenHelper.CARDS_ANSWER)));
				}else{
					card.setAnswer("");
				}
				//add the card to the reviewQuiz arrayList mdail 7-23-15
				Chapters.favorites.add(card);
				inCursor.moveToNext();
			}
		}else {
			Chapters.noFavorites = true;
		}
		inCursor.close();
		inCursor=null;
		return null;
	}
	//Convert to ChaptersInfo which is a list of the chapters name and count for each Chapter mdail 9-30-15
	private Void convetCursorToChaptersInfo(Cursor inCursor){
		if (inCursor.getCount() > 0) {
			inCursor.moveToFirst();

			while(!inCursor.isAfterLast()){
				Chapters.setChapterName(inCursor.getInt(0),inCursor.getString(1));
				Chapters.setChapterTotals(inCursor.getInt(0),inCursor.getInt(2));
				if(!inCursor.isAfterLast()){
					inCursor.moveToNext();
				}
			}
		}
		inCursor.close();
		inCursor=null;
		return null;
	}
	public void setDBVersion(int version) {
		open();
		database.execSQL("PRAGMA user_version = " + version);
		close();
	}
	
	public Integer getDBVersion() {
		Integer version = 0;
		open();
		version = database.getVersion();
		return version;
	}
	//Convet main data to Chapters class which in esence is an array of arrays mdail 5-18-15
		private Void convertCursorToChapters(Cursor inCursor){
			//was the function name below, I added the cursor[0]. array notation to the cursor variable to get it to work as doInBackground mdail 5-6-15
			//public long cursorToList(Cursor cursor){
			Boolean copyChapter = false;
			// holds indiviual row data mdail 5-8-15
			Cards card;
			// used to know when to change chapter arrays mdail 5-8-15
			lastChapterWas = 1;
			// hold the chapter data until it is sent to the Chapters class mdail 5-8-15
			cards = new ArrayList<Cards>();
			if (inCursor.getCount() > 0) {
				inCursor.moveToFirst();
				//if (!cursor.isAfterLast()){ duha that was dumb without the loop to count the first 30 mdail 5-7-15
				while(!inCursor.isAfterLast()){
					card = new Cards();
					//check to make sure the fields that can be null are not null and set the field to the value else set it 
					//to "" for strings mdail 5-5-15 
					card.setFavorite(inCursor.getInt(inCursor.getColumnIndexOrThrow(FlashCardsDBOpenHelper.CARDS_FAVORITE)));
					card.setChapter(inCursor.getInt(inCursor.getColumnIndexOrThrow(FlashCardsDBOpenHelper.CARDS_CHAPTER)));
					card.setId(inCursor.getInt(inCursor.getColumnIndexOrThrow(FlashCardsDBOpenHelper.CARDS_ID)));
					card.setChapter_name(inCursor.getString(inCursor.getColumnIndexOrThrow(FlashCardsDBOpenHelper.CARDS_CHAPTER_NAME)));
					if(!inCursor.isNull(inCursor.getColumnIndexOrThrow(FlashCardsDBOpenHelper.CARDS_QUESTION))){
						card.setQuestion(inCursor.getString(inCursor.getColumnIndexOrThrow(FlashCardsDBOpenHelper.CARDS_QUESTION)));
					}else{
						card.setQuestion("");
					}
					if(!inCursor.isNull(inCursor.getColumnIndexOrThrow(FlashCardsDBOpenHelper.CARDS_ANSWER))){
						card.setAnswer(inCursor.getString(inCursor.getColumnIndexOrThrow(FlashCardsDBOpenHelper.CARDS_ANSWER)));
					}else{
						card.setAnswer("");
					}
					if(lastChapterWas != card.getChapter()){
						copyChapter = false;
						// fill the chapter arrays in the Chapters class mdail 5-8-15
						copyChapter = fillChapterArraysOuter((ArrayList<Cards>) cards,lastChapterWas);
						//reset the cards to 0 to start adding records to the next chapter mdail 5-8-15
						cards.clear();
						cards = null;
						cards = new ArrayList<Cards>();
						//add the current record to the new cards which is the next chapter mdail 5-8-15
						cards.add(card);
						//set the lastChapterWas to the current chapter mdail 5-8-15
						lastChapterWas = card.getChapter();
					}else{
						//add data if the chapter number is still the same mdail 5-8-15
						cards.add(card);
					}
					// added as it was originaly moving to the second record before starting then it never move againg during
					// the count loop mdail 5-5-15
					if(!inCursor.isAfterLast()){
						inCursor.moveToNext();
						//after move are we now after last? if we are we should have chapter 20 complete and need to add it
						//to the chpaters class mdail 5-7-15
						if(inCursor.isAfterLast()){
							//Since Chapter 20 is the last Chapter !inCursor.isAfterLast() will happen while lastChapterWas != card.getChapter() never
							// will happen because there is no records for a Chapter 21 so this should create the chapter 21 array 
							// in the chapters class mdail 5-7-15 
							copyChapter = false;
							// fill the chapter arrays in the Chapters class mdail 5-8-15
							copyChapter = fillChapterArraysOuter((ArrayList<Cards>) cards,22);
							cards.clear();
							cards = null;
							card = null;
						}
					}else{
						//Since Chapter 21 is the last Chapter !inCursor.isAfterLast() will happen while lastChapterWas != card.getChapter() never
						// will happen because there is no records for a Chapter 22 so this should create the chapter 21 array 
						// in the chapters class mdail 5-7-15 
						copyChapter = false;
						// fill the chapter arrays in the Chapters class mdail 5-8-15
						copyChapter = fillChapterArraysOuter((ArrayList<Cards>) cards,22);
						cards.clear();
						cards = null;	
					}
				}
			}					
			//if the above does not work I may have to add Chapter 21 here prior to closing the cursor mdail 5-8-15
			if(cards != null && !cards.isEmpty()){
				//Since Chapter 21 is the last Chapter !inCursor.isAfterLast() will happen while lastChapterWas != card.getChapter() never
				// will happen because there is no records for a Chapter 22 so this should create the chapter 21 array 
				// in the chapters class mdail 5-7-15
				copyChapter = Chapters.setChapter22(cards);
				cards.clear();
				cards = null;
				card = null;
			}
			//close the cursor mdail 5-7-15
			inCursor.close();
			//set it to null mdail 5-7-15
			cursor = null;
			return null;
		}
		// refactored out of doInBackground this method saves the chapters all they are built to the correct array in the 
		// by sending the completed chapter list to allChapterArrays which is a static instance of the Chapters Class 
		// then calling the appropriate set Chapter function mdail 5-7-15
		private Boolean fillChapterArraysOuter(ArrayList<Cards> inCards,int chapterNu) {
			//Changed the break statement to returns as this should speed up this function for all
			//Chapters except 20, it is only a small improvement however any little bit could help mdail 5-8-15
			Boolean copyChapter = false;
			int chapter= chapterNu;
			switch(chapter){
			case 1:
				copyChapter = Chapters.setChapter1(inCards);		
				return copyChapter;
			case 2:
				copyChapter = Chapters.setChapter2(inCards);		
				return copyChapter;
			case 3:
				copyChapter = Chapters.setChapter3(inCards);		
				return copyChapter;
			case 4:
				copyChapter = Chapters.setChapter4(inCards);		
				return copyChapter;
			case 5:
				copyChapter = Chapters.setChapter5(inCards);		
				return copyChapter;
			case 6:
				copyChapter = Chapters.setChapter6(inCards);		
				return copyChapter;
			case 7:
				copyChapter = Chapters.setChapter7(inCards);		
				return copyChapter;
			case 8:
				copyChapter = Chapters.setChapter8(inCards);		
				return copyChapter;
			case 9:
				copyChapter = Chapters.setChapter9(inCards);		
				return copyChapter;
			case 10:
				copyChapter = Chapters.setChapter10(inCards);		
				return copyChapter;
			case 11:
				copyChapter = Chapters.setChapter11(inCards);		
				return copyChapter;
			case 12:
				copyChapter = Chapters.setChapter12(inCards);		
				return copyChapter;
			case 13:
				copyChapter = Chapters.setChapter13(inCards);		
				return copyChapter;
			case 14:
				copyChapter = Chapters.setChapter14(inCards);		
				return copyChapter;
			case 15:
				copyChapter = Chapters.setChapter15(inCards);		
				return copyChapter;
			case 16:
				copyChapter = Chapters.setChapter16(inCards);		
				return copyChapter;
			case 17:
				copyChapter = Chapters.setChapter17(inCards);		
				return copyChapter;
			case 18:
				copyChapter = Chapters.setChapter18(inCards);		
				return copyChapter;
			case 19:
				copyChapter = Chapters.setChapter19(inCards);		
				return copyChapter;
			case 20:
				copyChapter = Chapters.setChapter20(inCards);		
				return copyChapter;
			case 21:
				copyChapter = Chapters.setChapter21(inCards);		
				return copyChapter;
			case 22:
				copyChapter = Chapters.setChapter22(inCards);		
				return copyChapter;
			default:
				return false;
			}
		}
}
