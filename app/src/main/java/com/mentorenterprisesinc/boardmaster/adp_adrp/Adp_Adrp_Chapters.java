package com.mentorenterprisesinc.boardmaster.adp_adrp;

import android.app.Activity;
import android.content.Context;

import java.util.ArrayList;
import java.util.List;

public class Adp_Adrp_Chapters extends Activity {

    private static final String LOGTAG="Adp_Adrp_Chapters";
    public static Integer[] chapterEndsAt = new Integer[17];
    public static Integer[] selectedChaptersForFlash = new Integer[17];
    
    //Hold all of informations that makes up questions for flash cards mdail 9-30-15
    public static List<Adp_Adrp_Cards> chapter1 = new ArrayList<Adp_Adrp_Cards>();
    public static List<Adp_Adrp_Cards> chapter1_01 = new ArrayList<Adp_Adrp_Cards>();
    public static List<Adp_Adrp_Cards> chapter1_02 = new ArrayList<Adp_Adrp_Cards>();
    public static List<Adp_Adrp_Cards> chapter1_03 = new ArrayList<Adp_Adrp_Cards>();
    public static List<Adp_Adrp_Cards> chapter2 = new ArrayList<Adp_Adrp_Cards>();
    public static List<Adp_Adrp_Cards> chapter3 = new ArrayList<Adp_Adrp_Cards>();
    public static List<Adp_Adrp_Cards> chapter3_05 = new ArrayList<Adp_Adrp_Cards>();
    public static List<Adp_Adrp_Cards> chapter3_07 = new ArrayList<Adp_Adrp_Cards>();
    public static List<Adp_Adrp_Cards> chapter3_09 = new ArrayList<Adp_Adrp_Cards>();
    public static List<Adp_Adrp_Cards> chapter3_28 = new ArrayList<Adp_Adrp_Cards>();
    public static List<Adp_Adrp_Cards> chapter3_37 = new ArrayList<Adp_Adrp_Cards>();
    public static List<Adp_Adrp_Cards> chapter3_9 = new ArrayList<Adp_Adrp_Cards>();
    public static List<Adp_Adrp_Cards> chapter4 = new ArrayList<Adp_Adrp_Cards>();
    public static List<Adp_Adrp_Cards> chapter5 = new ArrayList<Adp_Adrp_Cards>();
    public static List<Adp_Adrp_Cards> chapter6 = new ArrayList<Adp_Adrp_Cards>();
    public static List<Adp_Adrp_Cards> chapter6_22 = new ArrayList<Adp_Adrp_Cards>();
    public static List<Adp_Adrp_Cards> chapter7 = new ArrayList<Adp_Adrp_Cards>();
    public static List<Adp_Adrp_Cards> favorites = new ArrayList<Adp_Adrp_Cards>();
    
    //Holds the first and last chapters selected for flash cards mdail 9-30-15
    public static Integer firstSelectedChapter = 0;
    public static Integer lastSelectedChapter = 0;
    
    //Variable to hold the data set by the data returned from the raw query total number of questions by
    //chapter and names mdail 10-1-15
    private static String chapter1Name;
    private static String chapter1_01Name;
    private static String chapter1_02Name;
    private static String chapter1_03Name;
    private static String chapter2Name;
    private static String chapter3Name;
    private static String chapter3_05Name;
    private static String chapter3_07Name;
    private static String chapter3_09Name;
    private static String chapter3_28Name;
    private static String chapter3_37Name;
    private static String chapter3_9Name;
    private static String chapter4Name;
    private static String chapter5Name;
    private static String chapter6Name;
    private static String chapter6_22Name;
    private static String chapter7Name;

    //For getting and holding the context
    public static Context context;
    public static Boolean favorite = false;
    public static Boolean noFavorites = true;

    //main constructor, only needs to be called once at the start and the class is open as long as the app is, it
    //also sets the local context so it can be used throughout the class as needed mdail 9-30-15
    public Adp_Adrp_Chapters(Context context1){
        context = context1;
    }
    
    //This was setting context however I moved it to the constructor mdail 10-1-15
//
//    @Override
//    public void onCreate() {
//    }

    //Returns the favorite for the selected questions in the selected chapter mdail 10-1-15
    public static Integer getChapterFavorite(Integer index,Integer chapter) {
        Integer data;
        ArrayList<Adp_Adrp_Cards> tempChapter;
        tempChapter = selectChapter(chapter);
        data=tempChapter.get(index).getFavorite();
        tempChapter = null;
        return data;
    }
    //Returns the Chapter # for the selected questions in the selected chapter mdail 10-1-15
    public static Integer getChapterChapter(Integer index,Integer chapter) {
        Integer data;
        ArrayList<Adp_Adrp_Cards> tempChapter;
        tempChapter = selectChapter(chapter);
        data=tempChapter.get(index).getChapter();
        tempChapter = null;
        return data;
    }
    //Returns the id # for the selected questions in the selected chapter mdail 10-1-15
    public static Integer getChapterId(Integer index,Integer chapter) {
        Integer data;
        ArrayList<Adp_Adrp_Cards> tempChapter;
        tempChapter = selectChapter(chapter);
        data=tempChapter.get(index).getId();
        tempChapter = null;
        return data;
    }
    //Returns the chapter name for the selected questions in the selected chapter mdail 10-1-15
    public static String getChapterChapter_name(Integer index,Integer chapter) {
        String data;
        ArrayList<Adp_Adrp_Cards> tempChapter;
        tempChapter = selectChapter(chapter);
        data=tempChapter.get(index).getChapter_name();
        tempChapter = null;
        return data;
    }
    //Returns the question for the selected questions in the selected chapter mdail 10-1-15
    public static String getChapterQuestion(Integer index,Integer chapter) {
        String data;
        ArrayList<Adp_Adrp_Cards> tempChapter;
        tempChapter = selectChapter(chapter);
        data=tempChapter.get(index).getQuestion();
        tempChapter = null;
        return data;
    }
    //Returns the answer for the selected questions in the selected chapter mdail 10-1-15
    public static String getChapterAnswer(Integer index,Integer chapter) {
        String data;
        ArrayList<Adp_Adrp_Cards> tempChapter;
        tempChapter = selectChapter(chapter);
        data=tempChapter.get(index).getAnswer();
        tempChapter = null;
        return data;
    }
    //Updates the cards object and the database with the Question, note this is only allowed for chapter 21 for which it return
    //true it returns false if it is called for any chapter other than 21 and does nothing mdail 10-1-15
    public static Boolean setChapterQuestion(Integer index,Integer chapter,String data) {
        ArrayList<Adp_Adrp_Cards> tempChapter;
        tempChapter = selectChapter(chapter);
        tempChapter.get(index).setQuestion(data);
        return true;
    }
    //Updates the cards object and the database with the Answer, note this is only allowed for chapter for which it return
    //true it returns false if it is called for any chapter other than 21 and does nothing mdail 10-1-15
    public static Boolean setChapterAnswer(Integer index,Integer chapter,String data) {
        ArrayList<Adp_Adrp_Cards> tempChapter;
        tempChapter = selectChapter(chapter);
        tempChapter.get(index).setAnswer(data);
        tempChapter = null;
        return true;
    }
    public static void setChapterFavorite(Integer index,Integer chapter,Integer data) {
        ArrayList<Adp_Adrp_Cards> tempChapter;
        tempChapter = selectChapter(chapter);
        tempChapter.get(index).setFavorite(data);
        tempChapter = null;
        return;
    }
    //select the proper chapter to be able to return and set the proper array without having to copy the array mdail 9-30-15
    public static ArrayList<Adp_Adrp_Cards> selectChapter(Integer chapterNumber){

        switch((int) chapterNumber){

            case 1:
                return (ArrayList<Adp_Adrp_Cards>) chapter1;
            case 2:
                return (ArrayList<Adp_Adrp_Cards>) chapter1_01;
            case 3:
                return (ArrayList<Adp_Adrp_Cards>) chapter1_02;
            case 4:
                return (ArrayList<Adp_Adrp_Cards>) chapter1_03;
            case 5:
                return (ArrayList<Adp_Adrp_Cards>) chapter2;
            case 6:
                return (ArrayList<Adp_Adrp_Cards>) chapter3;
            case 7:
                return (ArrayList<Adp_Adrp_Cards>) chapter3_05;
            case 8:
                return (ArrayList<Adp_Adrp_Cards>) chapter3_07;
            case 9:
                return (ArrayList<Adp_Adrp_Cards>) chapter3_09;
            case 10:
                return (ArrayList<Adp_Adrp_Cards>) chapter3_28;
            case 11:
                return (ArrayList<Adp_Adrp_Cards>) chapter3_37;
            case 12:
                return (ArrayList<Adp_Adrp_Cards>) chapter3_9;
            case 13:
                return (ArrayList<Adp_Adrp_Cards>) chapter4;
            case 14:
                return (ArrayList<Adp_Adrp_Cards>) chapter5;
            case 15:
                return (ArrayList<Adp_Adrp_Cards>) chapter6;
            case 16:
                return (ArrayList<Adp_Adrp_Cards>) chapter6_22;
            case 17:
                return (ArrayList<Adp_Adrp_Cards>) chapter7;
            default:
                //It should never get here if it does there is a problem, however it is required by the compiler mdail 9-30-15
                return (ArrayList<Adp_Adrp_Cards>) chapter1;
        }
    }

    //Set all the chapter array lists to the values for each chapter mdail 5-6-15
    public static Boolean setChapter1(List<Adp_Adrp_Cards> newChapter){
        //add all the entries from the incoming list to the chapter1 list and return true is it works mdail 5-6-15
        return chapter1.addAll(newChapter);
    }

    public static Boolean setChapter2(List<Adp_Adrp_Cards> newChapter){
        //add all the entries from the incoming list to the chapter1_01 list and return true is it works mdail 5-6-15
        return chapter1_01.addAll(newChapter);
    }

    public static Boolean setChapter3(List<Adp_Adrp_Cards> newChapter){
        //add all the entries from the incoming list to the chapter1_02 list and return true is it works mdail 5-6-15
        return chapter1_02.addAll(newChapter);
    }

    public static Boolean setChapter4(List<Adp_Adrp_Cards> newChapter){
        //add all the entries from the incoming list to the chapter1_03 list and return true is it works mdail 5-6-15
        return chapter1_03.addAll(newChapter);
    }
    public static Boolean setChapter5(List<Adp_Adrp_Cards> newChapter){
        //add all the entries from the incoming list to the chapter2 list and return true is it works mdail 5-6-15
        return chapter2.addAll(newChapter);
    }

    public static Boolean setChapter6(List<Adp_Adrp_Cards> newChapter){
        //add all the entries from the incoming list to the chapter3 list and return true is it works mdail 5-6-15
        return chapter3.addAll(newChapter);
    }

    public static Boolean setChapter7(List<Adp_Adrp_Cards> newChapter){
        //add all the entries from the incoming list to the chapter3_05 list and return true is it works mdail 5-6-15
        return chapter3_05.addAll(newChapter);
    }

    public static Boolean setChapter8(List<Adp_Adrp_Cards> newChapter){
        //add all the entries from the incoming list to the chapter3_07 list and return true is it works mdail 5-6-15
        return chapter3_07.addAll(newChapter);
    }

    public static Boolean setChapter9(List<Adp_Adrp_Cards> newChapter){
        //add all the entries from the incoming list to the chapter3_09 list and return true is it works mdail 5-6-15
        return chapter3_09.addAll(newChapter);
    }

    public static Boolean setChapter10(List<Adp_Adrp_Cards> newChapter){
        //add all the entries from the incoming list to the chapter3_28 list and return true is it works mdail 5-6-15
        return chapter3_28.addAll(newChapter);
    }

    public static Boolean setChapter11(List<Adp_Adrp_Cards> newChapter){
        //add all the entries from the incoming list to the chapter3_37 list and return true is it works mdail 5-6-15
        return chapter3_37.addAll(newChapter);
    }

    public static Boolean setChapter12(List<Adp_Adrp_Cards> newChapter){
        //add all the entries from the incoming list to the chapter3_9 list and return true is it works mdail 5-6-15
        return chapter3_9.addAll(newChapter);
    }

    public static Boolean setChapter13(List<Adp_Adrp_Cards> newChapter){
        //add all the entries from the incoming list to the chapter4 list and return true is it works mdail 5-6-15
        return chapter4.addAll(newChapter);
    }

    public static Boolean setChapter14(List<Adp_Adrp_Cards> newChapter){
        //add all the entries from the incoming list to the chapter5 list and return true is it works mdail 5-6-15
        return chapter5.addAll(newChapter);
    }
    public static Boolean setChapter15(List<Adp_Adrp_Cards> newChapter){
        //add all the entries from the incoming list to the chapter6 list and return true is it works mdail 5-6-15
        return chapter6.addAll(newChapter);
    }

    public static Boolean setChapter16(List<Adp_Adrp_Cards> newChapter){
        //add all the entries from the incoming list to the chapter6_22 list and return true is it works mdail 5-6-15
        return chapter6_22.addAll(newChapter);
    }

    public static Boolean setChapter17(List<Adp_Adrp_Cards> newChapter){
        //add all the entries from the incoming list to the chapter7 list and return true is it works mdail 5-6-15
        return chapter7.addAll(newChapter);
    }

    //set the name of the chapter by the chapter number 5-18-15
    public static String getChapterName(Integer chapterNumber){

        switch(chapterNumber){

            case 1:
                return chapter1Name;
            case 2:
                return chapter1_01Name;
            case 3:
                return chapter1_02Name;
            case 4:
                return chapter1_03Name;
            case 5:
                return chapter2Name;
            case 6:
                return chapter3Name;
            case 7:
                return chapter3_05Name;
            case 8:
                return chapter3_07Name;
            case 9:
                return chapter3_09Name;
            case 10:
                return chapter3_28Name;
            case 11:
                return chapter3_37Name;
            case 12:
                return chapter3_9Name;
            case 13:
                return chapter4Name;
            case 14:
                return chapter5Name;
            case 15:
                return chapter6Name;
            case 16:
                return chapter6_22Name;
            case 17:
                return chapter7Name;
            default:
                //It should never get here if it does there is a problem, however it is required by the compiler mdail 9-30-15
                return chapter7Name;
        }
    }

    //set the name of the chapter by the chapter number for the select chapter activity 9-30-15
    public static void setChapterName(Integer chapterNumber, String ChapterName){

        switch(chapterNumber){

            case 1:
                chapter1Name = ChapterName;
                break;
            case 2:
                chapter1_01Name = ChapterName;
                break;
            case 3:
                chapter1_02Name = ChapterName;
                break;
            case 4:
                chapter1_03Name = ChapterName;
                break;
            case 5:
                chapter2Name = ChapterName;
                break;
            case 6:
                chapter3Name = ChapterName;
                break;
            case 7:
                chapter3_05Name = ChapterName;
                break;
            case 8:
                chapter3_07Name = ChapterName;
                break;
            case 9:
                chapter3_09Name = ChapterName;
                break;
            case 10:
                chapter3_28Name = ChapterName;
                break;
            case 11:
                chapter3_37Name = ChapterName;
                break;
            case 12:
                chapter3_9Name = ChapterName;
                break;
            case 13:
                chapter4Name = ChapterName;
                break;
            case 14:
                chapter5Name = ChapterName;
                break;
            case 15:
                chapter6Name = ChapterName;
                break;
            case 16:
                chapter6_22Name = ChapterName;
                break;
            case 17:
                chapter7Name = ChapterName;
                break;
            default:
                //It should never get here if it does there is a problem, however it is required by the compiler mdail 9-30-15
                chapter7Name = ChapterName;
        }
    }
}
