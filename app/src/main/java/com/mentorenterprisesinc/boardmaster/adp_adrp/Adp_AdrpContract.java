package com.mentorenterprisesinc.boardmaster.adp_adrp;

import android.provider.BaseColumns;

/**
 * Created by Viktor Khon on 9/17/2017.
 */

public final class Adp_AdrpContract {

    public static class Adp_AdrpEntry implements BaseColumns {
        public static final String TABLE_NAME = "adp_adrp";
        public static final String COLUMN_ID = "id";
        public static final String COLUMN_FAVORITE = "favorite";
        public static final String COLUMN_CHAPTER = "chapter";
        public static final String COLUMN_CHAPTER_NAME = "chapter_name";
        public static final String COLUMN_QUESTION = "question";
        public static final String COLUMN_ANSWER = "answer";

    }
}
