package com.mentorenterprisesinc.boardmaster.adp_adrp;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import com.mentorenterprisesinc.boardmaster.SharedProcedures;
import com.mentorenterprisesinc.boardmaster.adp_adrp.Adp_AdrpContract.Adp_AdrpEntry;

import java.io.IOException;

/**
 * Created by Viktor Khon on 9/18/2017.
 */

public class Adp_AdrpDbOpenHelper extends SQLiteOpenHelper {

    private static final String LOGTAG = "Adp_AdrpDbOpenHelper";
    public static String DATABASE_PATH;
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "flash_cards.db";

    public Adp_AdrpDbOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        DATABASE_PATH = context.getFilesDir().getPath() + "/databases/";
    }

    String SQL_CREATE_ENTRIES  =
            "CREATE TABLE " + Adp_AdrpEntry.TABLE_NAME + " (" +
                    Adp_AdrpEntry.COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                    Adp_AdrpEntry.COLUMN_FAVORITE + " NUMERIC," +
                    Adp_AdrpEntry.COLUMN_CHAPTER + " NUMERIC," +
                    Adp_AdrpEntry.COLUMN_CHAPTER_NAME + " TEXT," +
                    Adp_AdrpEntry.COLUMN_QUESTION + " TEXT," +
                    Adp_AdrpEntry.COLUMN_ANSWER + " TEXT)";

    @Override
    public void onCreate(SQLiteDatabase db) {
                db.execSQL(SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String SQL_DELETE_ENTRIES =
                "DROP TABLE IF EXISTS " + Adp_AdrpEntry.TABLE_NAME;
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }
}
