package com.mentorenterprisesinc.boardmaster.adp_adrp;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.mentorenterprisesinc.boardmaster.Chapters;
import com.mentorenterprisesinc.boardmaster.R;
import com.mentorenterprisesinc.boardmaster.adp_adrp.Adp_AdrpContract.Adp_AdrpEntry;

import java.util.ArrayList;

public class Adp_Adrp_Select_Chapters extends Activity {

    private Adp_AdrpDbOpenHelper mDbHelper;
    private SQLiteDatabase db;
    private Cursor cursor;
    private ListView chaptersListView;
    private ArrayAdapter<String> adapter;
    private CheckBox chk_favorites;
    public ProgressBar progress;
    private ProgressBar pb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.select_chapters_activity);
        progress = (ProgressBar) findViewById(R.id.pb_select_subject);
        // TODO: Changed back to VISIBLE when done with the rest of the code
        progress.setVisibility(View.INVISIBLE);

        ArrayList<String> chapterlist = new ArrayList<String>();
        chapterlist = buildList();
        chaptersListView = (ListView) findViewById(R.id.lstv_selec_chapters);
        chaptersListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        adapter = new ArrayAdapter<String>(this, R.layout.modified_simple_list_item_multiple_choice, chapterlist);
        chaptersListView.setAdapter(adapter);

        // TODO: Need to finish the code to check for favorites for this specific table. Check Chapters Activity for code
        // TODO: It looks like data for favorite cards is received from FlashCardsDataSource...
        chk_favorites = (CheckBox) findViewById(R.id.chk_favorites);
        // Check if there are any questions saved as favorites. If not, grayout this box
        if (Chapters.noFavorites) {
            chk_favorites.setEnabled(false);
        } else {
            // If there are favorite questions, make this box clickable, but uncheck by default
            chk_favorites.setEnabled(true);
            chk_favorites.setChecked(false);
        }

        setViewListerners();

    }

    private void setViewListerners() {
        chk_favorites.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Make a listview onItemClicked listener that is called when Items are checked mdail 10-5-15
                if(chk_favorites.isChecked()) {
                    setAllNotSelected();
                }else {
                    setAllSelected();
                }
            }
        });

        chaptersListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(position == 0){
                    //if position 0 is clicked then we need to go though and make sure they are all selected mdail 10-2-15
                    setReverseAllSelected();
                    chk_favorites.setChecked(false);
                    //if position isn't 0 the just toggle selected and background color mdail 10-2-15
                }else{
                    //if the item does not have a check mark set the background to white and unchecked it mdail 10-2-15
                    if(!chaptersListView.isItemChecked(position)){
                        if(chaptersListView.isItemChecked(0)){
                            chaptersListView.setItemChecked(0, false);
                        }
                    }else{
                        //if 17 items are checked, this would mean that all items except 0 are select, so set all items checked mdail 10-2-15
                        if(chaptersListView.getCheckedItemCount() == 17){
                            if(!chaptersListView.isItemChecked(0)){
                                chaptersListView.setItemChecked(0, true);
                                chk_favorites.setChecked(false);
                            }
                        }
                    }
                }
                //make sure the favorites and a chapter can not be chosen at the same time mdail 10-2-15
                if(chaptersListView.getCheckedItemCount() >= 1){
                    chk_favorites.setChecked(false);
                }
            }
        });
    }

    // Build a new ArrayList<String> using Cursor vkhon 9/20/17
    private ArrayList<String> buildList() {
        ArrayList<String> chapterListTemp = new ArrayList<>();
        chapterListTemp.add("All");

        mDbHelper = new Adp_AdrpDbOpenHelper(this);
        // Open database that is already created and came with the app in ReadOnly mode
        db = SQLiteDatabase.openDatabase(mDbHelper.DATABASE_PATH + mDbHelper.DATABASE_NAME
                , null, SQLiteDatabase.OPEN_READONLY);

        String[] projections = {
                Adp_AdrpEntry.COLUMN_ID,
                Adp_AdrpEntry.COLUMN_CHAPTER,
                Adp_AdrpEntry.COLUMN_CHAPTER_NAME
        };

        // Get the Cursor from a adp_adrp table, by only calling for ID, Chapter, and Chapter Name columns
        cursor = db.query(
                Adp_AdrpEntry.TABLE_NAME,
                projections,
                null,
                null,
                null,
                null,
                null
        );

        int chapter = cursor.getColumnIndex(Adp_AdrpEntry.COLUMN_CHAPTER);
        int chapterName = cursor.getColumnIndex(Adp_AdrpEntry.COLUMN_CHAPTER_NAME);

        double currentCh;
        String chapName = null;
        String newChapName;

        while (cursor.moveToNext()) {
            currentCh = cursor.getDouble(chapter);
            chapName = cursor.getString(chapterName);

            chapterListTemp.add(currentCh + ": " + chapName);
            newChapName = chapName;

            // Check if Chapter names are the same, if so, skip it and move to the next row
            while (chapName.equals(newChapName)) {
                // While there is a valid row, keep moving to the next row
                if (cursor.moveToNext()) {
                    newChapName = cursor.getString(chapterName);
                    // Otherwise, change the name of a newChapName to stop the loop and close Cursor
                } else {
                    newChapName = null;
                    cursor.close();
                }
            }
        }
        return chapterListTemp;
    }

    //goes back to the chapter list if the back button is pressed mdail 10-2-15
    @Override
    public void onBackPressed() {
        finish();
    }

    //Switch the whole list to check/not checked with the appropriate background color mdail 10-2-15
    private void setReverseAllSelected(){
        if(chaptersListView.isItemChecked(0)){
            chk_favorites.setChecked(false);
            for(int i = 0; i < 18; i++){
                chaptersListView.setItemChecked(i, true);
            }
        }else{
            for(int i = 0; i < 18; i++){
                chaptersListView.setItemChecked(i, false);
            }
        }
    }

    //Set the whole list as not selected with the Gray background mdail 7-27-15
    private void setAllNotSelected(){
        //set the checked positions yellow and the unchecked white, only if they are visible mdail 7-27-15
        chaptersListView.setItemChecked(0, false);
        for(int i = 1; i < 18; i++){
            chaptersListView.setItemChecked(i, false);
        }
    }

    //Set the whole list as selected with the Yellow background mdail 5-21-15
    private void setAllSelected(){
        //set the checked positions yellow and the unchecked white, only if they are visible 6-10-15
        chaptersListView.setItemChecked(0, true);
        chk_favorites.setChecked(false);
        for(int i = 1; i < 18; i++){
            chaptersListView.setItemChecked(i, true);
        }
    }
}
