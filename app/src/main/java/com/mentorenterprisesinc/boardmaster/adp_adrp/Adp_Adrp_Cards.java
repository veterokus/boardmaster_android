package com.mentorenterprisesinc.boardmaster.adp_adrp;

import android.content.Context;

/**
 * Created by Viktor Khon on 9/16/2017.
 */

public class Adp_Adrp_Cards {

    private static final String LOGTAG="Adp_Adrp_Cards";
    public Context context;
    private Integer favorite;
    private Integer chapter;
    private Integer id;
    private String chapter_name;
    private String question;
    private String answer;

    public Integer getFavorite() {
        return favorite;
    }

    public void setFavorite(Integer favorite) {
        this.favorite = favorite;
    }

    public Integer getChapter() {
        return chapter;
    }

    public void setChapter(Integer chapter) {
        this.chapter = chapter;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getChapter_name() {
        return chapter_name;
    }

    public void setChapter_name(String chapter_name) {
        this.chapter_name = chapter_name;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}
